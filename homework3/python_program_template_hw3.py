#!/usr/bin/env python3
"""
Guidelines for program submission: 
    
A) To ensure readability by the grader, 
    please make sure the following submission format:

    1-- Please change the file name to python_program_fuck.py
        ---(The file name is case sensitive, make sure it's identical)---
               
    2-- The *.py file should be compressed to the root of the *.zip file
        e.g., when you submit your homework.zip, 
                Please compress the python files directly 
        ---(Do Not compress the directory!)---
                    aka, if you unzip your *.zip, *.py files should appear
                        in the directory of *.zip file. 

B) Your homework should follow the similar structure as this template

C) Keep in mind some minor points:
    
    0-- Your code should not have any debug error before submission!!!

    1-- make sure the function name is identical to that in the problem set (HW2)
            ---(function names are case sensitive! )---
    
    2-- The order of the function arguments should be the same as that in HW 

    2a-- The data type of the argument should be the same as that in template
                      
    3-- make sure your function return the value which the HW requested
    
    4-- make sure the order of the output arguments the same as those in template
    
    5-- Do not round up your output values
    
    6-- Do not use input function in your function
        
    7-- if you need uncommon modules, contact TA before submission or post it on Piazza

    8-- Please do not add any variable clearing command in the file, as this may terminate grader
    


If you had any question about the guideline, 
    contact TAs or post questions on piazza for response.
        
@author: Dan Li (lidan@ucsd.edu) & Yunhai Han (y8han@eng.ucsd.edu) at UCSD
@date: Jan 2021
"""



"""
The template starts from here
"""

# A432432 #PID

# import all modules here if you need any

# import numpy as np
# import random
# your file should always start from definition of functions 


def computeBFStree(AdjTable, start): 
    """ descriptions here """

    # define some variables etc.
    
    
    ##########
    #### Your main code here ####
    ##########
    
    
    # there should be two different types of results to return:  
    return 'AdjTable is invalid' # hint: check the type of the input, should be a dictionary or list of lists

    # or a vector of pointers parents describing the BFS tree rooted at start
    # equivalently, a list of nodes in visited order, start from the 'start node' 
    return # e.g. [ 'A', 'B', 'C', 'D']
    


def computeBFSpath(AdjTable, start,goal):
    """ descriptions here """

    # define some variables etc.
    
    
    ##########
    #### Your main code here ####
    ##########
    
    
    # there should be four different types of results to return:  
        
    return 'AdjTable is invalid'  # hint: check the type of the input

    return 'Starting point is the goal'
    return 'No path' 

    # or a path from start to goal along the BFS tree rooted at start
    return # a list expected e.g. [ start, 'A', 'B', 'C', 'D']
     
    
    
    
if __name__ == '__main__':
    """ 
    This is the place where you can test your function. 
    You can define variables, feed them into your function and check the output   
    """
    
    # AdjTable defined as a dictionary 
    
    AdjTable={ 'Tom' : ['Lee', 'Sam', 'Ben'], 
               'Lee' : ['Tom'],
               'Sam' : ['Tom', 'Ben'],
               'Ben' : ['Tom','Sam']}
    
    start='Lee'
    goal='Ben'
    
    myBFSTree=computeBFStree(AdjTable, start)
    print(myBFSTree)
    # solution should be a list: 
    #['Lee', 'Tom', 'Sam', 'Ben'] or 
    #['Lee', 'Tom', 'Ben', 'Sam']
    myBFSPath=computeBFSpath(AdjTable, start,goal)
    print(myBFSPath)
    # #['Lee', 'Tom', 'Ben']







