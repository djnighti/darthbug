# A15736154

import numpy as np
import random
from queue import Queue


def computeBFStree(AdjTable, start):
	""" descriptions here """
	# if not isinstance(AdjTable, dict) or not isinstance(AdjTable, list):
	# if type(AdjTable) != dict or type(AdjTable) != list:
	if type(AdjTable) == list:
		AdjTable = dict(enumerate(AdjTable1))
	if type(AdjTable) == dict:
		pass
	else:
		print(type(AdjTable))
		return 'AdjTable is invalid'  # hint: check the type of the input, should be a dictionary or list of lists
	q = []
	q.append(start)
	node_list = []
	parents_list = []
	# parents_list.append(start)
	for nodes, connections in AdjTable.items():
		node_list.append(nodes)
	for node in range(0, len(node_list)):
		parents_list.append(None)
	# print(parents_list)
	# print(node_list)
	# parents_dic = dict(enumerate(parents_list))
	parents_dic = {node_list[i]: parents_list[i] for i in range(len(node_list))}
	parents_dic[start] = start
	# print(parents_dic)
	count = 0
	visited = [start]
	while None in parents_dic.values():
		while not len(q) == 0:
			v = q.pop(0)
			# print(f"this is V on every iteration: {v}")
			# print(f"this is Q on every iteration: {q}")
			connections = AdjTable.get(v)
			# print(f"parent, connections: {v}, {connections}")
			for connection in connections:
				parent = parents_dic.get(connection)
				# print(f"connections: {connections}")
				# print(f"test parent: {parent}")
				if parent is None:
					parents_dic[connection] = v
					q.append(connection)
					# print(f"new Queue: {q}")
					visited.append(connection)
		search = 1
		while search:
			for key, value in parents_dic.items():
				if value is None:
					parents_dic[key] = key
					q.append(key)
					print(parents_dic)
					search = 0

	# print(parents_dic)
	# or a vector of pointers parents describing the BFS tree rooted at start
	# equivalently, a list of nodes in visited order, start from the 'start node'
	print(parents_dic)
	return visited # e.g. [ 'A', 'B', 'C', 'D']


def computeBFSpath(AdjTable, start, goal):
	""" descriptions here """

	if type(AdjTable) == list:
		AdjTable = dict(enumerate(AdjTable))
	if type(AdjTable) == dict:
		pass
	else:
		return 'AdjTable is invalid'
	if start == goal:
		return 'Starting point is the goal'

	q = []
	q.append(start)
	node_list = []
	parents_list = []
	# parents_list.append(start)
	for nodes, connections in AdjTable.items():
		node_list.append(nodes)
	for node in range(0, len(node_list)):
		parents_list.append(None)
	# print(parents_list)
	# print(node_list)
	# parents_dic = dict(enumerate(parents_list))
	parents_dic = {node_list[i]: parents_list[i] for i in range(len(node_list))}
	parents_dic[start] = start
	# print(parents_dic)
	visited = [start]
	while not len(q) == 0:
		v = q.pop(0)
		# print(f"this is V on every iteration: {v}")
		# print(f"this is Q on every iteration: {q}")
		connections = AdjTable.get(v)
		# print(f"parent, connections: {v}, {connections}")
		for connection in connections:
			parent = parents_dic.get(connection)
			# print(f"connections: {connections}")
			# print(f"test parent: {parent}")
			if parent is None:
				parents_dic[connection] = v
				q.append(connection)
			if connection == goal:
				visited.append(connection)
				# print(parents_dic)
				return visited
		visited.append(connection)
	# print(parents_dic)
	return parents_dic #'No path'


if __name__ == '__main__':
	""" 
	This is the place where you can test your function. 
	You can define variables, feed them into your function and check the output   
	"""

	# AdjTable defined as a dictionary

	AdjTable = {'Tom': ['Lee', 'Sam', 'Ben'],
	            'Lee': ['Tom'],
	            'Sam': ['Tom', 'Ben'],
	            'Ben': ['Tom', 'Sam'],
	            'Dom': ['Ron'],
	            'Ron': ['Dom']}
	AdjTable1 = [[1,2], [0,3], [0], [1]]
	start1 = 1
	# enum_adjTable = dict(enumerate(AdjTable1))
	# print(enum_adjTable)
	start = 'Lee'
	goal = 'Dom'
	# nodes = AdjTable.keys()
	# connections = AdjTable.values()
	# nodesAndConnections = AdjTable.items()

	# print((list(nodes)))
	# print(len(list(connections)))
	# print(type((list(nodesAndConnections))))
	# print(type(AdjTable))
	# if type(AdjTable1) == list:
	# 	print('this is a list')
	# if type(AdjTable) == dict:
	# 	print('this is a dictionary')
	# else:
	# 	print('This did not work')
	#
	# for nodes, connections in AdjTable.items():
	# 	print(nodes)


	myBFSTree = computeBFStree(AdjTable1, start1)
	print(myBFSTree)
	# solution should be a list:
	# ['Lee', 'Tom', 'Sam', 'Ben'] or
	# ['Lee', 'Tom', 'Ben', 'Sam']
	# myBFSPath = computeBFSpath(AdjTable, start, goal)
	# print(myBFSPath)
	# print(None in myBFSPath.values())
# #['Lee', 'Tom', 'Ben']







