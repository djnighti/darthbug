# A14636467 #PID
# Worked with Dominic Nightingale
# import all modules here if you need any

# import numpy as np
# import random
# your file should always start from definition of functions 

def listToDict(st):
    op = { i : st[i] for i in range(0, len(st) ) }
    return op

def computeBFStree(AdjTable, start): 
    """ descriptions here """

    # keep track of all visited nodes
    explored = []
    # keep track of nodes to be checked
    queue = [start]
    
    result = isinstance(AdjTable, (dict,list))
    if result == False:
    # there should be two different types of results to return:  
        return 'AdjTable is invalid' # hint: check the type of the input, should be a dictionary or list of lists
    
    if isinstance(AdjTable, list):
        AdjTable = listToDict(AdjTable) 
   
    # keep looping until there are nodes still to be checked
    while queue:
        # pop shallowest node (first node) from queue
        node = queue.pop(0)
        if node not in explored:
            # add node to list of checked nodes
            explored.append(node)
            neighbours = AdjTable[node]
 
            # add neighbours of node to queue
            for neighbour in neighbours:
                queue.append(neighbour)
    # or a vector of pointers parents describing the BFS tree rooted at start
    # equivalently, a list of nodes in visited order, start from the 'start node' 
    return explored # e.g. [ 'A', 'B', 'C', 'D']


def computeBFSpath(AdjTable, start, goal):
    """ descriptions here """

    
    # there should be four different types of results to return:  
    # keep track of explored nodes
    explored = []
    # keep track of all the paths to be checked
    queue = [[start]]
 
    result = isinstance(AdjTable, (dict,list))
    if result == False:
    # there should be two different types of results to return:  
        return 'AdjTable is invalid' # hint: check the type of the input, should be a dictionary or list of lists
    
    if isinstance(AdjTable, list):
        AdjTable = listToDict(AdjTable) 
        
    # return path if start is goal
    if start == goal:
        return 'Starting point is the goal'
    # keeps looping until all possible paths have been checked
    while queue:
        # pop the first path from the queue
        path = queue.pop(0)
        # get the last node from the path
        node = path[-1]
        if node not in explored:
            neighbours = AdjTable[node]
            # go through all neighbour nodes, construct a new path and
            # push it into the queue
            for neighbour in neighbours:
                new_path = list(path)
                new_path.append(neighbour)
                queue.append(new_path)
                # return path if neighbour is goal
                if neighbour == goal:
                    return new_path              
            # mark node as explored
            explored.append(node)
    return 'No path' 
     
    
    
    
if __name__ == '__main__':
    """ 
    This is the place where you can test your function. 
    You can define variables, feed them into your function and check the output   
    """
    
    # AdjTable defined as a dictionary 
    
    AdjTable={ 'Tom' : ['Lee', 'Sam', 'Ben'], 
               'Lee' : ['Tom'],
               'Sam' : ['Tom', 'Ben'],
               'Ben' : ['Tom','Sam']}
    
    start='Lee'
    goal='Ben'
    
    myBFSTree=computeBFStree(AdjTable, start)
    print(myBFSTree)
    # solution should be a list: 
    #['Lee', 'Tom', 'Sam', 'Ben'] or 
    #['Lee', 'Tom', 'Ben', 'Sam']
    myBFSPath=computeBFSpath(AdjTable, start,goal)
    print(myBFSPath)
    # #['Lee', 'Tom', 'Ben']