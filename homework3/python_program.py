# A15736154

import numpy as np
import random
from queue import Queue


def computeBFStree(AdjTable, start):
    """
        1. check data type of AdjTable and convert to dictionary type if needed.
        2. initialize Queue
        3. create new dictionary of all nodes and their parents (initially all none besides start node)
        4. perform BFS search while the queue is not empty
        5. get nodal connections and search if their parents are equal to None and set parents accordingly
        6. keep track of visted nodes and return a list of the visited nodes
    """

    if type(AdjTable) == list:
        AdjTable = dict(enumerate(AdjTable))
    if type(AdjTable) == dict:
        pass
    else:
        # print(type(AdjTable))
        return 'AdjTable is invalid'  # hint: check the type of the input, should be a dictionary or list of lists

    q = [start]
    node_list = []
    parents_list = []

    for nodes, connections in AdjTable.items():
        node_list.append(nodes)
    for node in range(0, len(node_list)):
        parents_list.append(None)

    parents_dic = {node_list[i]: parents_list[i] for i in range(len(node_list))}
    parents_dic[start] = start
    visited = [start]

    while not len(q) == 0:
        v = q.pop(0)
        connections = AdjTable.get(v)
        for connection in connections:
            parent = parents_dic.get(connection)
            if parent is None:
                parents_dic[connection] = v
                q.append(connection)
                visited.append(connection)

    return visited


def computeBFSpath(AdjTable, start, goal):
    """
        1. check data type of AdjTable and convert to dictionary type if needed.
        2. initialize Queue
        3. create new dictionary of all nodes and their parents (initially all none besides start node)
        4. perform BFS search while the queue is not empty
        5. get nodal connections and search if their parents are equal to None or the goal
        6. if goal reached, return visted nodes, else continue until goal is reached or stop if no path found
    """

    if type(AdjTable) == list:
        AdjTable = dict(enumerate(AdjTable))
    if type(AdjTable) == dict:
        pass
    else:
        return 'AdjTable is invalid'
    if start == goal:
        return 'Starting point is the goal'

    q = [start]
    node_list = []
    parents_list = []

    for nodes, connections in AdjTable.items():
        node_list.append(nodes)
    for node in range(0, len(node_list)):
        parents_list.append(None)

    parents_dic = {node_list[i]: parents_list[i] for i in range(len(node_list))}
    parents_dic[start] = start
    visited = []
    while not len(q) == 0:
        v = q.pop(0)
        connections = AdjTable.get(v)
        for connection in connections:
            parent = parents_dic.get(connection)
            if parent is None:
                parents_dic[connection] = v
                q.append(connection)
            if connection == goal:
                path = extract_path(start, goal, parents_dic)
                return path
        visited.append(v)

    return 'No path'


def extract_path(start_node, goal_node, parents_dictionary):
    parent = parents_dictionary.get(goal_node)
    path = [goal_node]
    path.insert(0, parent)
    while parent != start_node:
        parent = parents_dictionary.get(parent)
        path.insert(0, parent)
    return path


if __name__ == '__main__':
    """ 
    Tested list of strings and numbers, both are working as intended. 
    """

    # AdjTable defined as a dictionary
    AdjTable = {'Tom': ['Lee', 'Sam', 'Ben'],
                'Lee': ['Tom'],
                'Sam': ['Tom', 'Ben'],
                'Ben': ['Tom', 'Sam']}

    start = 'Lee'
    goal = 'Ben'
    # BFS solution ['Lee', 'Tom', 'Ben']

    AdjTable_num = [[1, 2], [0, 2, 3], [0, 1], [1]]
    start_num = 2
    goal_num = 3
    # BFS path solution [2, 1, 3]

    strings = 0
    numbers = 1

    AdjTable_option = numbers

    if AdjTable_option == strings:
        myBFSTree = computeBFStree(AdjTable, start)
        myBFSPath = computeBFSpath(AdjTable, start, goal)
    elif AdjTable_option == numbers:
        myBFSTree = computeBFStree(AdjTable_num, start_num)
        myBFSPath = computeBFSpath(AdjTable_num, start_num, goal_num)
    print(myBFSTree)
    print(myBFSPath)
