#!/usr/bin/env python3

# A15736154 #PID

# import all modules here if you need any
import matplotlib.pyplot as plt
import numpy as np

def sample_normal_distribution(mu, var):
    np.random.seed(1)
    ### operations
    a = np.sqrt(var)
    z = mu + 0.5*np.sum(np.random.uniform(-a, a, 12))
    return z

def controlled_noisy_motion(x0, u):
    x = [x0]
    
    # operations here
    mu = 0
    var = 1
    w = []
    for iter in range(len(u)):
        y = sample_normal_distribution(mu, var)
        w.append(y)
        x = np.append(x, x[iter] + u[iter] + w[iter])
    return x #x could be a numpy array or a list



if __name__ == '__main__':
    
    # Define obstacle polygon in the counter clockwise direction
    mu = 100
    var = 25
    y = []
    for x in range(10000):
        x = sample_normal_distribution(mu, var)
        y.append(x)
    # Visualization here
    count, bins, ignored = plt.hist(y, 100)
    plt.show()
    
    x0= 20
    u = [1,1,1,1,5,-1,-1,-1,-1,-5]
    x = controlled_noisy_motion(x0, u)