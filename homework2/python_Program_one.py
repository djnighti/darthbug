# A15736154
import numpy as np
import random


def create_board():
    board = np.zeros((3, 3), dtype=int)
    return board


def place(board, player, position):
    row = position[0]
    col = position[1]
    if board[row, col] == 0:
        board[row, col] = player
    return board


def possibilities(board):
    indices = np.where(board == 0)
    rows = indices[0]
    cols = indices[1]
    num_points = len(rows)
    ind = []
    for n in range(0, num_points):
        ind.append((rows[n], cols[n]))
    return ind


def random_place(board, player):
    # random.seed(1)
    poss = possibilities(board)
    randomChoice = random.choice(poss)
    board = place(board, player, randomChoice)
    return board


def repeat(n):
    board = create_board()
    player1 = 1
    player2 = 2
    m = 0
    # random.seed(1)
    while m < n:
        board = random_place(board, player1)
        board = random_place(board, player2)
        m += 1
    return board


if __name__ == '__main__':
    n = 4
    board = repeat(n)
    winner = evaluate(board)
    print(board)
    print(f"The winner is player: {winner}")

