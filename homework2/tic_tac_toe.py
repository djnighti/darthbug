import numpy as np
import random


def create_board():
	emptyBoard = np.zeros((3, 3))
	return emptyBoard


def place(board, player, position):
	newBoard = np.copy(board)
	row = position[0]
	col = position[1]
	if newBoard[row,col] == 0:
		newBoard[row,col] = player
	return newBoard


def possibilities(board):
	indices = np.where(board == 0)
	rows = indices[0]
	cols = indices[1]
	num_points = len(rows)
	cn = []
	for n in range(0, num_points):
		cn.append((rows[n], cols[n]))
	return cn


def random_place(board, player):
	poss = possibilities(board)
	randomChoice = random.choice(poss)
	newBoard = place(board, player, randomChoice)
	return newBoard


def repeat(n):
	newBoard = create_board()
	poss = possibilities(newBoard)
	player1 = 1
	player2 = 2
	m = 0
	while m < n:
		randomChoice = random.choice(poss)
		newBoard = place(newBoard, player1, randomChoice)
		poss = possibilities(newBoard)
		randomChoice = random.choice(poss)
		newBoard = place(newBoard, player2, randomChoice)
		poss = possibilities(newBoard)
		m+=1
	return newBoard


results = repeat(4)
print(results)
