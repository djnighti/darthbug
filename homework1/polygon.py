import math
import numpy as np
from matplotlib import pyplot as plt
# from shapely.geometry.polygon import LinearRing, Polygon
import time

def convert_xy_to_r_theta(list):
    r, theta = [[], []]
    try:
        for i in range(0, len(list)):
            x = list[i][0]
            y = list[i][1]
            r.append(math.sqrt(x ** 2 + y ** 2))
            theta.append(math.atan2(y, x) * 180 / math.pi)
    except:
        x = list[0]
        y = list[1]
        r.append(math.sqrt(x**2 + y**2))
        theta.append(math.atan2(y, x) * 180 / math.pi)
    return([r,theta])


def reference_position(point, list):
    tx = point[0]
    ty = point[1]
    translation_mat = np.array([[1,0,tx],[0,1,ty],[0,0,1]])
    point_mat = np.array([tx,ty,1])
    list_mat = np.asarray(list)
    list_mat = np.append(list_mat, [[1], [1], [1], [1]], axis=1)
    new_point_pos = np.linalg.solve(translation_mat,point_mat)
    new_list_pos = np.array([[1,1,1]])

    for i in [0,1,2,3]:
        new_list_pos_index = np.linalg.solve(translation_mat,list_mat[i])
        new_list_pos = np.append(new_list_pos, [new_list_pos_index], axis=0)

    new_list_pos = np.delete(new_list_pos, 0, axis=0)
    new_list_pos = np.delete(new_list_pos, 2, axis=1)
    new_point_pos = np.delete(new_point_pos, 2, axis=0)

    final_point = new_point_pos.tolist()
    new_list_pos_list = new_list_pos.tolist()
    return final_point, new_list_pos_list


def find_two_closest_points(list):
    list_copy = list.copy()
    rmin = min(list_copy[0])
    adjecnt_points = []

    for i in range(1):
        min_index = [i for i, value in enumerate(list_copy[0]) if value == rmin]
        min_index_val = min_index[0]
        min_val = list_copy[0][min_index[0]]
    for j in [-1,1]:
        try:
            adjecnt_points.append(list_copy[0][min_index_val + j])
        except:
            adjecnt_points.append(list_copy[0][min_index_val - 1])
            adjecnt_points.append(list_copy[0][0]) # first item in list

    d1 = abs(adjecnt_points[0] - min_val)
    d2 = abs(adjecnt_points[1] - min_val)
    if d1 <= d2:
        next_min_index_val = min_index_val-1
    else:
        next_min_index_val = min_index_val + 1
    return([min_index_val,next_min_index_val])


def computeLineThroughTwoPoints(list):
    p1x = list[0][0]
    p1y = list[0][1]
    p2x = list[1][0]
    p2y = list[1][1]
    try:
        m = (p2y-p1y) / (p2x-p1x)
        normalized_coeff = math.sqrt((m**2) + 1)
        a = m / normalized_coeff
        b = -1 / normalized_coeff
        c = (m*p1x - p1y) / normalized_coeff
    except ZeroDivisionError:
        a = 1
        b = 0
        c = p1x
    return [a, b, c]


def computeDistancePointToLine(point, list):
    p1x = list[0][0]
    p1y = list[0][1]
    p2x = list[1][0]
    p2y = list[1][1]
    q1x = point[0]
    q1y = point[1]

    py_min = min(p1y, p2y)
    px_min = min(p1x, p2x)
    py_max = max(p1y, p2y)
    px_max = max(p1x, p2x)
    try:
        m = (p2y-p1y) / (p2x-p1x)
        if m != 0:
            mq = -1 / m

            y_intercept = mq * q1x - q1y
            q2x = (mq * q1x - m * p1x + p1y - q1y) / (mq - m)
            q2y = m * q2x + y_intercept
            d = math.sqrt((q2x - q1x) ** 2 + (q2y - q1y) ** 2)
        else:  # zero slope
            diff_y1 = abs(q1y - py_min)
            diff_y2 = abs(q1y - py_max)
            if diff_y1 < diff_y2:
                d = q1y - py_min
                print("q is directly below line")
            else:
                d = q1y - py_max
                print("q is directly above line ")
    except ZeroDivisionError:
        d = q1x - p1x
    return d


def computeDistancePointToSegment(point, list):
    p1x = list[0][0]
    p1y = list[0][1]
    p2x = list[1][0]
    p2y = list[1][1]
    # print([p1x,p1y,p2x,p2y])
    q1x = point[0]
    q1y = point[1]
    # print([q1x,q1y])

    py_min = min(p1y, p2y)
    px_min = min(p1x, p2x)
    py_max = max(p1y, p2y)
    px_max = max(p1x, p2x)

    if (py_min <= q1y <= py_max) or (px_min <= q1x <= px_max):
        try:
            m = (p2y-p1y) / (p2x-p1x)
            if m != 0:
                mq = -1 / m
                y_intercept = mq * q1x - q1y
                q2x = (mq * q1x - m * p1x + p1y - q1y) / (mq - m)
                q2y = m * q2x + y_intercept
                d = math.sqrt((q2x - q1x) ** 2 + (q2y - q1y) ** 2)
                print("q is inbetween/side line segment")
            else:  # zero slope
                diff_x1 = abs(q1y - px_min)
                diff_x2 = abs(q1y - px_max)
                if diff_x1 < diff_x2:  # to the left
                    d = q1x - px_min
                    print("q is closer to the left point")
                else:  # to the right
                    d = q1x - px_max
                    print("q is closer to the right point")
        except ZeroDivisionError:  # infinite slope
            print([q1y,py_min,py_max])
            diff_y1 = abs(q1y - py_min)
            diff_y2 = abs(q1y - py_max)
            if diff_y1 < diff_y2:
                d = q1y - py_min
                print("q is directly below of line segment")
            else:
                d = q1y - py_max
                print("q is directly above of line segment")
    elif q1y < p2y and q1y < p1y:
        d = math.sqrt((q1x - px_min) ** 2 + (q1y - py_min) ** 2)
        print("q is below line segment")
    elif q1y > p2y and q1y > p1y:
        d = math.sqrt((q1x - px_max) ** 2 + (q1y - py_max) ** 2)
        print("q is above line segment")
    return d



def computeDistancePointToPolygon(point, list):
    q_new, poly_new = reference_position(point, list)
    r_theta_poly = convert_xy_to_r_theta(poly_new)
    points_to_poly = find_two_closest_points(r_theta_poly)
    closest_points = [poly_new[points_to_poly[0]], poly_new[points_to_poly[1]]]
    dist = computeDistancePointToSegment(q_new, closest_points)
    return dist

start = time.time()
Onew = [[2.015, 1.42], [1.575, 1.76], [1.695, 3.34], [1.915, 3.58], [7.245, 3.68], [8.079, 3.334], [8.425, 2.5], [8.079,1.6656], [7.245, 1.32], [4.859, 2.74], [4.859, 2.74]]
# O5 = [  ]

obs = [[2,2], [2,4], [4,4], [4,2]]
obs = [[2.015,1.42], [1.675,1.76], [1.675,3.34], [1.915,3.58], [4.895,3.58], [4.859,2.74]]
polygon2 = obs
q = [1,1]


q_new, poly_new = reference_position(q, polygon2)
print("q_new : ", q_new)
print("poly_new : ", poly_new)

r_theta_poly = convert_xy_to_r_theta(poly_new)
r_theta_q = convert_xy_to_r_theta(q_new)

closest_elements = find_two_closest_points(r_theta_poly)
closest_points = [polygon2[closest_elements[0]], polygon2[closest_elements[1]]]
closest_average = [((closest_points[0][0] + closest_points[1][0]) / 2), ((closest_points[0][1] + closest_points[1][1]) / 2)]
print("closest points:",closest_points)

line_coeff = computeLineThroughTwoPoints(closest_points)
print("computeLineThroughTwoPoints : ", line_coeff)

A,B,C = computeLineThroughTwoPoints([closest_points[0], q])
print("computeLineThroughTwoPoints q_to_poly: ", [A,B,C])

distLine = computeDistancePointToLine(q,closest_points)
print("computeDistancePointToLine : ", distLine)

distSeg = computeDistancePointToSegment(q,closest_points)
print("computeDistancePointToSegment : ",distSeg)

distPoly = computeDistancePointToPolygon(q, polygon2)
print("computeDistancePointToPolygon : ",distPoly)


# Ax + By = C
# y = -(A/B)*x + C/B
if closest_points[0][0] < q[0]:
    x = np.arange(closest_points[0][0], q[0],0.01)
else:
    x = np.arange(q[0],closest_points[0][0], 0.01)
try:
    y = -(A/B)*x + C/B
except ZeroDivisionError:
    x = [C/A, C/A]
    y = [q[1],closest_points[0][1]]

square = plt.Polygon(polygon2, fc="r")
plt.scatter(q[0],q[1])
plt.plot(x,y)
plt.gca().add_patch(square)
plt.axis([0, 10, 0, 10])
end = time.time()
print(f"Runtime of the program is: {end - start} seconds")
plt.show()