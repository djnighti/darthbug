import math
import numpy as np
from matplotlib import pyplot as plt
import time


def convert_list_to_float_list(list):
    new_list = []
    for n in range(len(list)):
        flolat_point = [float(i) for i in list[n]]
        new_list.append(flolat_point)
    return new_list


def convert_xy_to_r_theta(list):
    r, theta = [[], []]
    try:
        for i in range(0, len(list)):
            x = list[i][0]
            y = list[i][1]
            r.append(math.sqrt(x ** 2 + y ** 2))
            theta.append(math.atan2(y, x) * 180 / math.pi)
    except:
        x = list[0]
        y = list[1]
        r.append(math.sqrt(x ** 2 + y ** 2))
        theta.append(math.atan2(y, x) * 180 / math.pi)
    return ([r, theta])


def reference_position(point, list):

    """
        Translate point to the origin by (tx, ty) then translate the polygon
        with n vertices the same (tx,ty)

    """

    tx = point[0]
    ty = point[1]
    list_length = len(list)
    translation_mat = np.array([[1, 0, tx], [0, 1, ty], [0, 0, 1]])
    point_mat = np.array([tx, ty, 1])
    list_mat = np.asarray(list)
    ones_vec = np.array([[1]])
    for n in range(0, list_length - 1):
        ones_vec = np.append(ones_vec, [[1]], axis=0)
    list_mat = np.append(list_mat, ones_vec, axis=1)
    np_translated_point = np.linalg.solve(translation_mat, point_mat)
    np_translated_list = np.array([[1, 1, 1]])

    for i in range(0, list_length):
        np_translated_list_index = np.linalg.solve(translation_mat, list_mat[i])
        np_translated_list = np.append(np_translated_list, [np_translated_list_index], axis=0)

    np_translated_list = np.delete(np_translated_list, 0, axis=0)
    np_translated_list = np.delete(np_translated_list, 2, axis=1)
    np_translated_point = np.delete(np_translated_point, 2, axis=0)

    translated_point = np_translated_point.tolist()
    translated_list = np_translated_list.tolist()
    return translated_point, translated_list


def check_if_inside_obstacle(point, list):
    """
        defining if point is in polygon.
            if inside: fail
            if outside: success
    """

    fail = 0
    success = 1

    """
        Initializing point q and polygon.
            create new data array that account for using the same point 
            in 2 different calculations. (See handwritten derivation)
    """
    theta_vec = []
    P_new = []
    P_new.append(list[0])
    for n in range(1, len(list)):
        P_new.append(list[n])
        P_new.append(list[n])
    P_new.append(list[0])

    """
        Calculate angles from q to 2 distinct points on polygon (from piazza)
            -ZeroDivisionError: point is on boundary of polygon causing zero-magnitude
            -on every iteration of for loop, delete previous 2 points to get the two new need points
    """

    try:
        for n in range(len(list)):
            Pn1x = P_new[0][0]
            Pn1y = P_new[0][1]
            Pn2x = P_new[1][0]
            Pn2y = P_new[1][1]
            Pn1 = [Pn1x, Pn1y]
            Pn2 = [Pn2x, Pn2y]

            x_mag = computeDistancePointToPoint([Pn1, point])
            y_mag = computeDistancePointToPoint([Pn2, point])
            xy_mag = x_mag * y_mag
            x_dot_y_1 = (Pn1x * Pn2x + Pn1y * Pn2y)
            theta_n = math.acos(x_dot_y_1 / xy_mag) * 180 / math.pi
            if theta_n == 0.0 and xy_mag == 0:
                break
            theta_vec.append(theta_n)
            P_new.pop(0)
            P_new.pop(0)
        theta_sum = sum(theta_vec)
        if theta_n == 0.0:
            result = fail
        elif theta_sum >= 360:
            result = fail
        else:
            result = success
    except ZeroDivisionError:
        result = fail
    return result


def find_closest_points(list, num_points=3):
    """

    :param list: Takes list a points and returns closest point (min_point) to the origin (because q has been
        translated there)
    :param num_points: default to = 3 finds also the two neighbor points to that min_point, if !=3 will only
        return the midpoint
    :return: index values for the minimum point in list and by default, the indices for the two neighbor points

    """
    try:
        list_copy = list.copy()
    except:
        list_copy = list.copy()
    rmin = min(list_copy[0])
    min_indices = []

    for i in range(1):
        min_index = [i for i, value in enumerate(list_copy[0]) if value == rmin]
        min_index_val = min_index[0]
        min_indices.append(min_index_val)
    if num_points == 3:
        if ((min_index_val + 1) < len(list_copy[0])):
            for j in [-1, 1]:
                min_indices.append(min_index_val + j)
        else:
            min_indices.append(min_index_val - 1)
            min_indices.append(0)  # first item in list
    else:
        min_indices = min_index_val
    return min_indices


def computeDistancePointToPoint(list):
    p1x = list[0][0]
    p1y = list[0][1]
    p2x = list[1][0]
    p2y = list[1][1]
    d = math.sqrt((p2x - p1x) ** 2 + (p2y - p1y) ** 2)
    return d


def computeLineThroughTwoPoints(list):
    list = convert_list_to_float_list(list)
    p1x = list[0][0]
    p1y = list[0][1]
    p2x = list[1][0]
    p2y = list[1][1]
    try:
        m = (p2y - p1y) / (p2x - p1x)
        normalized_coeff = math.sqrt((m ** 2) + 1)
        a = m / normalized_coeff
        b = -1 / normalized_coeff
        c = (m * p1x - p1y) / normalized_coeff
    except ZeroDivisionError:
        a = 1
        b = 0
        c = p1x
    return [a, b, c]


def computeDistancePointToLine(point, list):
    list = convert_list_to_float_list(list)
    point = convert_list_to_float_list([point])[0]
    print("point", point)
    print("list", list)
    p1x = list[0][0]
    p1y = list[0][1]
    p2x = list[1][0]
    p2y = list[1][1]

    q1x = point[0]
    q1y = point[1]

    try:
        m = (p2y - p1y) / (p2x - p1x)
        print(m)
        if m != 0:  # q is inbetween/to the side of the line segment
            mq = -1 / m
            y_intercept = q1y - mq * q1x
            q2x = (mq * q1x - m * p1x + p1y - q1y) / (mq - m)
            q2y = mq * q2x + y_intercept
            q2 = [q2x, q2y]
            d = math.sqrt((q2x - q1x) ** 2 + (q2y - q1y) ** 2)
            print("test 1 q2", q2)
            print("test 1 q1", point)
        else:  # Zero slope
            q2x = q1x
            q2y = p1y
            q2 = [q2x, q2y]
            d = computeDistancePointToPoint([q2, point])
            print("test 2")
    except ZeroDivisionError:  # infinite slope
        q2x = p1x
        q2y = q1y
        q2 = [q2x, q2y]
        d = computeDistancePointToPoint([q2, point])
        print("test 3")

    return d, q2


def computeDistancePointToSegment(point, list):
    list = convert_list_to_float_list(list)
    point = convert_list_to_float_list([point])[0]
    print("point", point)
    print("list", list)
    P1x = list[0][0]
    P1y = list[0][1]
    P2x = list[1][0]
    P2y = list[1][1]
    Pn1 = [P1x, P1y]
    Pn2 = [P2x, P2y]
    q1x = point[0]
    q1y = point[1]
    px_min = min(P1x, P2x)
    py_min = min(P1y, P2y)
    px_max = max(P1x, P2x)
    py_max = max(P1y, P2y)

    d1 = computeDistancePointToPoint([Pn1, point])
    d2 = computeDistancePointToPoint([Pn2, point])
    dc = min(d1,d2)

    if (py_min <= q1y <= py_max) or (px_min <= q1x <= px_max):
        d = computeDistancePointToLine(point, list)[0]
    else:
        d = dc
    d1_d_diff = abs(d1 - d)
    d2_d_diff = abs(d2 - d)

    if d1_d_diff == d2_d_diff:
        w = 0
    elif d1_d_diff <= d2_d_diff:
        w = 1
    elif d2_d_diff <= d1_d_diff:
        w = 2
    return d, w


def computeDistancePointToPolygon(point, list):
    list = convert_list_to_float_list(list)
    point = convert_list_to_float_list([point])[0]
    point_translated, list_translated = reference_position(point, list)
    result = check_if_inside_obstacle(point_translated, list_translated)

    if result:
        r_theta_list = convert_xy_to_r_theta(list_translated)
        points_to_poly = find_closest_points(r_theta_list)
        closest_3_points = [list_translated[points_to_poly[0]], list_translated[points_to_poly[1]], list_translated[points_to_poly[2]]]
        closest_points_1 = [closest_3_points[0], closest_3_points[1]]
        closest_points_2 = [closest_3_points[0], closest_3_points[2]]

        closest_point_average_1 = [((closest_points_1[0][0] + closest_points_1[1][0]) / 2), ((closest_points_1[0][1] + closest_points_1[1][1]) / 2)]
        closest_point_average_2 = [((closest_points_2[0][0] + closest_points_2[1][0]) / 2), ((closest_points_2[0][1] + closest_points_2[1][1]) / 2)]

        dist_closest_point_average_1 = computeDistancePointToPoint([closest_point_average_1, point_translated])
        dist_closest_point_average_2 = computeDistancePointToPoint([closest_point_average_2, point_translated])

        if dist_closest_point_average_1 < dist_closest_point_average_2:
            closest_points = closest_points_1
        else:
            closest_points = closest_points_2

        dist = computeDistancePointToSegment(point_translated, closest_points)[0]
    else:
        dist = "Point is inside the polygon. Try starting from another location"
    return dist


start = time.time()
do_loop = 0;

if do_loop:
    x_rot = np.arange(0, 9, 0.1)
    x_flip = np.flip(x_rot)
    y_rot = np.sqrt(9 - (x_rot - 3) ** 2) + 4
    y_rot_neg = -1*np.sqrt(9 - (x_flip - 3) ** 2) + 4
    x_rot = np.append(x_rot, x_flip)
    y_rot = np.append(y_rot, y_rot_neg)
else:
    x_rot = np.asarray([5])
    y_rot = np.asarray([9])

for n in range(0, x_rot.size):
    P = [[2, 3], [2, 5], [4, 5], [3, 2]]
    # P = [[0, 0], [0, 1], [1, 1], [1, 0]]
    q = [x_rot[n], y_rot[n]]

    q_translated, P_translated = reference_position(q, P)
    result = check_if_inside_obstacle(q_translated, P_translated)
    if result:
        r_theta_poly = convert_xy_to_r_theta(P_translated)
        r_theta_q = convert_xy_to_r_theta(q_translated)
        closest_elements = find_closest_points(r_theta_poly, 3)
        closest_3_points = [P[closest_elements[0]], P[closest_elements[1]], P[closest_elements[2]]]
        closest_points_1 = [closest_3_points[0], closest_3_points[1]]
        closest_points_2 = [closest_3_points[0], closest_3_points[2]]
        print("closest 3 points :", closest_3_points)

        distLine_1, intersection_point_1 = computeDistancePointToLine(q, closest_points_1)
        distLine_2, intersection_point_2 = computeDistancePointToLine(q, closest_points_2)
        closest_point_average_1 = [((closest_points_1[0][0] + closest_points_1[1][0]) / 2), ((closest_points_1[0][1] + closest_points_1[1][1]) / 2)]
        closest_point_average_2 = [((closest_points_2[0][0] + closest_points_2[1][0]) / 2), ((closest_points_2[0][1] + closest_points_2[1][1]) / 2)]
        dist_closest_point_average_1 = computeDistancePointToPoint([closest_point_average_1, q])
        dist_closest_point_average_2 = computeDistancePointToPoint([closest_point_average_2, q])

        if dist_closest_point_average_1 < dist_closest_point_average_2:
            closest_points = closest_points_1
            intersection_point = intersection_point_1
            closest_points_average = closest_point_average_1
            print("computeDistancePointToLine 1 : ", distLine_1)
            print("intersection_point_1 : ", intersection_point_1)
        else:
            closest_points = closest_points_2
            intersection_point = intersection_point_2
            closest_points_average = closest_point_average_2
            print("computeDistancePointToLine 2 : ", distLine_2)
            print("intersection_point_2 : ", intersection_point_2)

        A, B, C = computeLineThroughTwoPoints([intersection_point, q])
        print("computeLineThroughTwoPoints, q intersecting line: ", [A, B, C])

        A_s, B_s, C_s = computeLineThroughTwoPoints([closest_points[0], q])
        print("computeLineThroughTwoPoints, q to closest point: ", [A_s, B_s, C_s])

        A_p, B_p, C_p = computeLineThroughTwoPoints(closest_points)
        print("computeLineThroughTwoPoints, 2 closest points to q: ", [A_p, B_p, C_p])

        distSeg, w = computeDistancePointToSegment(q, closest_points)
        print("closest_points", closest_points)
        print("computeDistancePointToSegment : ", distSeg)

        distPoly = computeDistancePointToPolygon(q, P)
        print("computeDistancePointToPolygon : ", distPoly)
        # [x_poly, y_poly],
        # Ax + By = C
        # y = -(A/B)*x + C/B
        # plot line from q to closest point on the line created from 2 closest points
        x_intersection = intersection_point[0]
        y_intersection = intersection_point[1]
        if x_intersection < q[0]:
            xq = np.arange(x_intersection, q[0], 0.01)
        else:
            xq = np.arange(q[0], x_intersection, 0.01)
        try:
            yq = -(A / B) * xq + C / B

        except ZeroDivisionError:
            xq = [C / A, C / A]
            yq = [q[1], y_intersection]

        # plot line from q to closest point on the line segment
        x_mid = closest_points[0][0]
        y_mid = closest_points[0][1]
        if x_mid < q[0]:
            xs = np.arange(x_mid, q[0], 0.01)
        else:
            xs = np.arange(q[0], x_mid, 0.01)
        try:
            ys = -(A_s / B_s) * xs + C_s / B_s

        except ZeroDivisionError:
            xs = [C_s / A_s, C_s / A_s]
            ys = [q[1], y_mid]

        # plot line of the two closest points
        try:
            xp = np.arange(0, 10, 0.01)
            yp = -(A_p / B_p) * xp + C_p / B_p
        except ZeroDivisionError:
            xp = [C_p / A_p, C_p / A_p]
            yp = [0, 10]

        square = plt.Polygon(P, fc="r")
        plt.cla()
        plt.gca().add_patch(square)
        x_closest = [closest_points[0][0], closest_points[1][0]]
        y_closest = [closest_points[0][1], closest_points[1][1]]
        plt.scatter(x_intersection, y_intersection)
        print(x_intersection, y_intersection)
        plt.scatter(x_closest, y_closest)
        plt.scatter(q[0], q[1])
        plt.plot(xq, yq)
        plt.plot(xp, yp)
        plt.plot(xs, ys)
        plt.plot(x_rot, y_rot)
        plt.axis([0, 10, 0, 10])
        plt.savefig(f'pic{n}.png')
    else:
        print("Point is inside the polygon. Try starting from another location")
end = time.time()

print(f"Runtime of the program is: {end - start} seconds")
if do_loop or not result:
    pass
else:
    plt.show()
