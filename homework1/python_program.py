# A15736154
import math
import time


def convert_list_to_float_list(list):
    new_list = []
    for n in range(len(list)):
        flolat_point = [float(i) for i in list[n]]
        new_list.append(flolat_point)
    return new_list


def computeDistancePointToPoint(list):
    tolerance = 1E-8
    p1x = list[0][0]
    p1y = list[0][1]
    p2x = list[1][0]
    p2y = list[1][1]
    d = math.sqrt((p2x - p1x) ** 2 + (p2y - p1y) ** 2)
    if d < tolerance:
        d = 0
    return d


def computeLineThroughTwoPoints(list):
    list = convert_list_to_float_list(list)
    p1x = list[0][0]
    p1y = list[0][1]
    p2x = list[1][0]
    p2y = list[1][1]
    try:
        m = (p2y - p1y) / (p2x - p1x)
        normalized_coeff = math.sqrt((m ** 2) + 1)
        a = m / normalized_coeff
        b = -1 / normalized_coeff
        c = (m * p1x - p1y) / normalized_coeff
    except ZeroDivisionError:
        a = 1
        b = 0
        c = p1x
    return [a, b, c]


def computeDistancePointToLine(point, list):
    tolerance = 1E-8
    list = convert_list_to_float_list(list)
    point = convert_list_to_float_list([point])[0]
    p1x = list[0][0]
    p1y = list[0][1]
    p2x = list[1][0]
    p2y = list[1][1]

    q1x = point[0]
    q1y = point[1]

    try:
        m = (p2y - p1y) / (p2x - p1x)
        if m != 0:  # q is inbetween/to the side of the line segment
            mq = -1 / m
            y_intercept = q1y - mq * q1x
            q2x = (mq * q1x - m * p1x + p1y - q1y) / (mq - m)
            q2y = mq * q2x + y_intercept
            d = math.sqrt((q2x - q1x) ** 2 + (q2y - q1y) ** 2)
        else:  # Zero slope
            q2x = q1x
            q2y = p1y
            q2 = [q2x, q2y]
            d = computeDistancePointToPoint([q2, point])
    except ZeroDivisionError:  # infinite slope
        q2x = p1x
        q2y = q1y
        q2 = [q2x, q2y]
        d = computeDistancePointToPoint([q2, point])
    if d < tolerance:
        d = 0
    return d


def computeDistancePointToSegment(point, list):
    list = convert_list_to_float_list(list)
    point = convert_list_to_float_list([point])[0]
    P1x = list[0][0]
    P1y = list[0][1]
    P2x = list[1][0]
    P2y = list[1][1]
    Pn1 = [P1x, P1y]
    Pn2 = [P2x, P2y]
    q1x = point[0]
    q1y = point[1]
    px_min = min(P1x, P2x)
    py_min = min(P1y, P2y)
    px_max = max(P1x, P2x)
    py_max = max(P1y, P2y)

    d1 = computeDistancePointToPoint([Pn1, point])
    d2 = computeDistancePointToPoint([Pn2, point])
    dc = min(d1,d2)

    if (py_min <= q1y <= py_max) or (px_min <= q1x <= px_max):
        d = computeDistancePointToLine(point, list)
    else:
        d = dc
    d1_d_diff = abs(d1 - d)
    d2_d_diff = abs(d2 - d)

    if d1_d_diff == d2_d_diff:
        w = 0
    elif d1_d_diff <= d2_d_diff:
        w = 1
    elif d2_d_diff <= d1_d_diff:
        w = 2
    return d, w


start = time.time()
q = [2, 3]
P = [[2, 3], [1,6]]

A, B, C = computeLineThroughTwoPoints(P)
print("computeLineThroughTwoPoints q_to_line: ", [A, B, C])

d_line = computeDistancePointToLine(q, P)
print("computeDistancePointToLine: ", d_line)

d_seg = computeDistancePointToSegment(q, P)
print("computeDistancePointToSegment: ", d_seg)

end = time.time()
print(f"Runtime of the program is: {end - start} seconds")