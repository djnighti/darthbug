# A15736154
import math
import numpy as np
import time


def convert_list_to_float_list(list):
    new_list = []
    for n in range(len(list)):
        flolat_point = [float(i) for i in list[n]]
        new_list.append(flolat_point)
    return new_list


def convert_xy_to_r_theta(list):
    r, theta = [[], []]
    try:
        for i in range(0, len(list)):
            x = list[i][0]
            y = list[i][1]
            r.append(math.sqrt(x ** 2 + y ** 2))
            theta.append(math.atan2(y, x) * 180 / math.pi)
    except:
        x = list[0]
        y = list[1]
        r.append(math.sqrt(x ** 2 + y ** 2))
        theta.append(math.atan2(y, x) * 180 / math.pi)
    return ([r, theta])


def reference_position(point, list):

    """
        Translate point to the origin by (tx, ty) then translate the polygon
        with n vertices the same (tx,ty)

    """

    tx = point[0]
    ty = point[1]
    list_length = len(list)
    translation_mat = np.array([[1, 0, tx], [0, 1, ty], [0, 0, 1]])
    point_mat = np.array([tx, ty, 1])
    list_mat = np.asarray(list)
    ones_vec = np.array([[1]])
    for n in range(0, list_length - 1):
        ones_vec = np.append(ones_vec, [[1]], axis=0)
    list_mat = np.append(list_mat, ones_vec, axis=1)
    np_translated_point = np.linalg.solve(translation_mat, point_mat)
    np_translated_list = np.array([[1, 1, 1]])

    for i in range(0, list_length):
        np_translated_list_index = np.linalg.solve(translation_mat, list_mat[i])
        np_translated_list = np.append(np_translated_list, [np_translated_list_index], axis=0)

    np_translated_list = np.delete(np_translated_list, 0, axis=0)
    np_translated_list = np.delete(np_translated_list, 2, axis=1)
    np_translated_point = np.delete(np_translated_point, 2, axis=0)

    translated_point = np_translated_point.tolist()
    translated_list = np_translated_list.tolist()
    return translated_point, translated_list


def check_if_inside_obstacle(point, list):
    """
        defining if point is in polygon.
            if inside: fail
            if outside: success
    """

    fail = 0
    success = 1

    """
        Initializing point q and polygon.
            create new data array that account for using the same point 
            in 2 different calculations. (See handwritten derivation)
    """
    theta_vec = []
    P_new = []
    P_new.append(list[0])
    for n in range(1, len(list)):
        P_new.append(list[n])
        P_new.append(list[n])
    P_new.append(list[0])

    """
        Calculate angles from q to 2 distinct points on polygon (from piazza)
            -ZeroDivisionError: point is on boundary of polygon causing zero-magnitude
            -on every iteration of for loop, delete previous 2 points to get the two new need points
    """

    try:
        for n in range(len(list)):
            Pn1x = P_new[0][0]
            Pn1y = P_new[0][1]
            Pn2x = P_new[1][0]
            Pn2y = P_new[1][1]
            Pn1 = [Pn1x, Pn1y]
            Pn2 = [Pn2x, Pn2y]

            x_mag = computeDistancePointToPoint([Pn1, point])
            y_mag = computeDistancePointToPoint([Pn2, point])
            xy_mag = x_mag * y_mag
            x_dot_y_1 = (Pn1x * Pn2x + Pn1y * Pn2y)
            theta_n = math.acos(x_dot_y_1 / xy_mag) * 180 / math.pi
            if theta_n == 0.0 and xy_mag == 0:
                break
            theta_vec.append(theta_n)
            P_new.pop(0)
            P_new.pop(0)
        theta_sum = sum(theta_vec)
        if theta_n == 0.0:
            result = fail
        elif theta_sum >= 360:
            result = fail
        else:
            result = success
    except ZeroDivisionError:
        result = fail
    return result


def find_closest_points(list, num_points=3):
    """

    :param list: Takes list a points and returns closest point (min_point) to the origin (because q has been
        translated there)
    :param num_points: default to = 3 finds also the two neighbor points to that min_point, if !=3 will only
        return the midpoint
    :return: index values for the minimum point in list and by default, the indices for the two neighbor points

    """
    try:
        list_copy = list.copy()
    except:
        list_copy = list.copy()
    rmin = min(list_copy[0])
    min_indices = []

    for i in range(1):
        min_index = [i for i, value in enumerate(list_copy[0]) if value == rmin]
        min_index_val = min_index[0]
        min_indices.append(min_index_val)
    if num_points == 3:
        if ((min_index_val + 1) < len(list_copy[0])):
            for j in [-1, 1]:
                min_indices.append(min_index_val + j)
        else:
            min_indices.append(min_index_val - 1)
            min_indices.append(0)  # first item in list
    else:
        min_indices = min_index_val
    return min_indices


def computeDistancePointToPoint(list):
    p1x = list[0][0]
    p1y = list[0][1]
    p2x = list[1][0]
    p2y = list[1][1]
    d = math.sqrt((p2x - p1x) ** 2 + (p2y - p1y) ** 2)
    return d


def computeDistancePointToLine(point, list):
    list = convert_list_to_float_list(list)
    point = convert_list_to_float_list([point])[0]
    p1x = list[0][0]
    p1y = list[0][1]
    p2x = list[1][0]
    p2y = list[1][1]

    q1x = point[0]
    q1y = point[1]

    try:
        m = (p2y - p1y) / (p2x - p1x)
        if m != 0:  # q is inbetween/to the side of the line segment
            mq = -1 / m
            y_intercept = q1y - mq * q1x
            q2x = (mq * q1x - m * p1x + p1y - q1y) / (mq - m)
            q2y = mq * q2x + y_intercept
            q2 = [q2x, q2y]
            d = math.sqrt((q2x - q1x) ** 2 + (q2y - q1y) ** 2)
        else:  # Zero slope
            q2x = q1x
            q2y = p1y
            q2 = [q2x, q2y]
            d = computeDistancePointToPoint([q2, point])
    except ZeroDivisionError:  # infinite slope
        q2x = p1x
        q2y = q1y
        q2 = [q2x, q2y]
        d = computeDistancePointToPoint([q2, point])

    return d, q2


def computeDistancePointToSegment(point, list):
    list = convert_list_to_float_list(list)
    point = convert_list_to_float_list([point])[0]
    P1x = list[0][0]
    P1y = list[0][1]
    P2x = list[1][0]
    P2y = list[1][1]
    Pn1 = [P1x, P1y]
    Pn2 = [P2x, P2y]
    q1x = point[0]
    q1y = point[1]
    px_min = min(P1x, P2x)
    py_min = min(P1y, P2y)
    px_max = max(P1x, P2x)
    py_max = max(P1y, P2y)

    d1 = computeDistancePointToPoint([Pn1, point])
    d2 = computeDistancePointToPoint([Pn2, point])
    dc = min(d1,d2)

    if (py_min <= q1y <= py_max) or (px_min <= q1x <= px_max):
        d = computeDistancePointToLine(point, list)[0]
    else:
        d = dc
    d1_d_diff = abs(d1 - d)
    d2_d_diff = abs(d2 - d)

    if d1_d_diff == d2_d_diff:
        w = 0
    elif d1_d_diff <= d2_d_diff:
        w = 1
    elif d2_d_diff <= d1_d_diff:
        w = 2
    return d, w


def computeDistancePointToPolygon(point, list):
    list = convert_list_to_float_list(list)
    point = convert_list_to_float_list([point])[0]
    point_translated, list_translated = reference_position(point, list)
    result = check_if_inside_obstacle(point_translated, list_translated)

    if result:
        r_theta_list = convert_xy_to_r_theta(list_translated)
        points_to_poly = find_closest_points(r_theta_list)
        closest_3_points = [list_translated[points_to_poly[0]], list_translated[points_to_poly[1]], list_translated[points_to_poly[2]]]
        closest_points_1 = [closest_3_points[0], closest_3_points[1]]
        closest_points_2 = [closest_3_points[0], closest_3_points[2]]

        closest_point_average_1 = [((closest_points_1[0][0] + closest_points_1[1][0]) / 2), ((closest_points_1[0][1] + closest_points_1[1][1]) / 2)]
        closest_point_average_2 = [((closest_points_2[0][0] + closest_points_2[1][0]) / 2), ((closest_points_2[0][1] + closest_points_2[1][1]) / 2)]

        dist_closest_point_average_1 = computeDistancePointToPoint([closest_point_average_1, point_translated])
        dist_closest_point_average_2 = computeDistancePointToPoint([closest_point_average_2, point_translated])

        if dist_closest_point_average_1 < dist_closest_point_average_2:
            closest_points = closest_points_1
        else:
            closest_points = closest_points_2

        dist = computeDistancePointToSegment(point_translated, closest_points)[0]
    else:
        dist = "Point is inside the polygon. Try starting from another location"
    return dist

start = time.time()
q = [0, 3]
P = [[2, 3], [2, 5], [4, 5], [3, 2]]
d = computeDistancePointToPolygon(q, P)
print(d)
end = time.time()
print(f"Runtime of the program is: {end - start} seconds")
