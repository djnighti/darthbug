#!/usr/bin/env python3
"""
Guidelines for program submission:

A) To ensure readability by the grader,
    please make sure the following submission format:

    1-- Please do not change the file name (leave as python_program.py)
        ---(The file name is case sensitive, make sure it's identical)---

    ---FAIL TO FOLLOW INSTRUCTION A) MAY COST YOU SOME POINTS---


B) Your homework should follow the similar structure as this template

If you had any question about the guideline,
    contact TAs or post questions on piazza for response.

@author: Dan Li (lidan@ucsd.edu) & Yunhai Han (y8han@eng.ucsd.edu) at UCSD
@date: Mar 2021
"""

"""
The template starts from here
"""

# A15736154 #PID
# Ronald Law

# import all modules here if you need any
import matplotlib.pyplot as plt
import numpy as np
import math


def sample_normal_distribution(mu, sigma):
    size = 1
    sigma = np.sqrt(sigma)
    u = np.random.uniform(size=size)
    v = np.random.uniform(size=size)
    z = np.sqrt(-2 * np.log(u)) * np.cos(2 * np.pi * v)
    z = mu + z * sigma
    return z


def predict(x_t, u_t, alpha):
    """ Sample odometry motion model.
    Arguments:
        x -- pose of the robot before moving [x, y, theta]
        u -- odometry reading obtained from the robot [rot1, rot2, trans]
        a -- noise parameters of the motion model [a1, a2, a3, a4]
    """
    x = x_t[0]
    y = x_t[1]
    theta = x_t[2]

    d1_rot = u_t[0]
    d2_rot = u_t[1]
    d_trans = u_t[2]

    alpha_1 = alpha[0]
    alpha_2 = alpha[1]
    alpha_3 = alpha[2]
    alpha_4 = alpha[2]

    mean = 0

    d1_rot_var = alpha_1 * d1_rot**2 + alpha_2 * d2_rot**2
    d_trans_var = alpha_3 * d_trans**2 + alpha_4 * (d1_rot**2 + d2_rot**2)
    d2_rot_var = alpha_1 * d1_rot**2 + alpha_2 * d2_rot**2

    d1_rot_hat = d1_rot - sample_normal_distribution(mean, d1_rot_var)
    d_trans_hat = d_trans - sample_normal_distribution(mean, d_trans_var)
    d2_rot_hat = d2_rot - sample_normal_distribution(mean, d2_rot_var)

    x_prime = x + d_trans_hat * math.cos(theta + d1_rot_hat)
    y_prime = y + d_trans_hat * math.sin(theta + d1_rot_hat)
    theta_prime = theta + d1_rot_hat + d2_rot_hat
    x_t_plus_1 = [x_prime, y_prime, theta_prime]

    return x_t_plus_1


if __name__ == '__main__':

    # mu = 100
    # var = 25
    # x = sample_normal_distribution(mu, var)
    # # Visualization here
    #
    # x = [2, 4, 0]
    # plt.plot(x[0], x[1], "go")
    X_t_plus_1 = []
    Y_t_plus_1 = []

    # change to 5000 for the solution of (c)
    num_samples = 50000
    # area
    for i in range(0, num_samples):
        x = [2, 4, 0]
        u = [(np.pi / 2), 0, 1]
        alpha = [0.1, 0.1, 0.01, 0.01]
        x_t_plus_1 = predict(x, u, alpha)
        x_t_plus_1[0] = x_t_plus_1[0][0]
        x_t_plus_1[1] = x_t_plus_1[1][0]
        x_t_plus_1[2] = x_t_plus_1[2][0]
        X_t_plus_1.append(x_t_plus_1[0])
        Y_t_plus_1.append(x_t_plus_1[1])
        plt.xlabel("x-position [m]")
        plt.ylabel("y-position [m]")
    plt.scatter(X_t_plus_1, Y_t_plus_1, s=1, c='r')
    print(x_t_plus_1)
    plt.show()



