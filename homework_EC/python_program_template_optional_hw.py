#!/usr/bin/env python3
"""
Guidelines for program submission: 
    
A) To ensure readability by the grader, 
    please make sure the following submission format:

    1-- Please change the file name to python_program_fuck.py
        ---(The file name is case sensitive, make sure it's identical)---
               
    2-- Please submit only the python_program_fuck.py to the autograder
        descriptions of the function should be added in .py file as comments 
         
    3-- Please do NOT assign random seed (i.e., random.seed(1) ) in any function 
	(the one we provided in "if __file__==__main__:" is fine)
               
                        
B) Your homework should follow the similar structure as this template

C) Keep in mind some minor points:
    
    0-- Your code should not have any debug error before submission!!!

    1-- make sure the function name is identical to that in the problem set (HW2)
            ---(function names are case sensitive! )---
    
    2-- The order of the function arguments should be the same as that in HW 

    2a-- The data type of the argument should be the same as that in template
                      
    3-- make sure your function return the value which the HW requested
    
    4-- make sure the order of the output arguments the same as those in template
    
    5-- Do not round up your output values
    
    6-- Do not use input function in your function
        
    7-- if you need uncommon modules, contact TA before submission or post it on Piazza
    


If you had any question about the guideline, 
    contact TAs or post questions on piazza for response.
        
@author: Dan Li (lidan@ucsd.edu) & Yunhai Han (y8han@eng.ucsd.edu) at UCSD
@date: Jan 2021
"""



"""
The template starts from here
"""

# A432432 #PID

# import all modules here if you need any more
import numpy as np
import random
# your file should always start from definition of functions 


def row_win(board, player):
    """ Add illustrations here, if needed """
    
    # define variables you need
    
    # your program 
    
    # return True or False 
    return  # True  / False 
    

def col_win(board, player):
    """ Add illustrations here, if needed """
    
    # define variables you need
    
    # your program 
    
    # return True or False 
    return  # True  / False 
    

def diag_win(board, player):
    """ Add illustrations here, if needed """
    
    # define variables you need
    
    # add your code here 
    
    # return True or False 
    return  # True  / False 
   


def evaluate(board):
    """ Add illustrations here, if needed """
        
    winner = 0
    for player in [1, 2]:
        #add your code here!
        pass
    
    if np.all(board != 0) and winner == 0:
        winner = -1
        
    return winner


def play_game():
    """ Add illustrations here, if needed """
          
    # add your code here 
    
    # return 1 or 2 to reflect the winning player, or -1 if it is a draw
    return  #

    
def play_game_1000():
    """ Play game 1000 times and return results """
          
    results=[]
    # add your code here 
    
    
    # return 1 or 2 to reflect the winning player, or -1 if it is a draw
    return  results


def play_strategic_game():
    """ Add illustrations here, if needed """
          
    # add your code here 
    
    # return 1 or 2 to reflect the winning player, or -1 if it is a draw
    return  #

    
def play_strategic_game_1000():
    """ Play strategic game 1000 times and return results """
          
    results=[]
    # add your code here 
    
    
    # return 1 or 2 to reflect the winning player, or -1 if it is a draw
    return  results



"""
Functions from previous exercises
We provide these functions for convenience
Feel free to use your own here
"""

def create_board():
    board = np.zeros((3,3), dtype = int)
    return board

def place(board,player,position):
    if board[position] == 0:
        board[position] = player
    return board

def possibilities(board):
    ind = []
    z = np.array(np.where(board == 0))
    s = z.shape[1]
    for i in range(s):
        ind.append((z[(0,i)],z[(1,i)]))
    return ind

def random_place(board,player):
    selection = possibilities(board)
    position = random.choice(selection)
    board = place(board,player,position)
    return board


    
if __name__ == '__main__':
    """ 
    This is the place where you can test your function. 
    You can define variables, feed them into your function and check the output   
    """
    random.seed(10000)

    board = create_board()
    
    board = place(board, 1, (0,0))  
    
    empty_positions = possibilities(board)
    
    board = random_place(board, 1)
 
    # add whatever you need to test your new functions
