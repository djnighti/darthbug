# A15736154
import numpy as np
import random


def create_board():
    board = np.zeros((3, 3), dtype=int)
    return board


def place(board, player, position):
    row = position[0]
    col = position[1]
    if board[row, col] == 0:
        board[row, col] = player
    return board


def possibilities(board):
    indices = np.where(board == 0)
    rows = indices[0]
    cols = indices[1]
    num_points = len(rows)
    ind = []
    for n in range(0, num_points):
        ind.append((rows[n], cols[n]))
    return ind


def random_place(board, player):
    poss = possibilities(board)
    randomChoice = random.choice(poss)
    board = place(board, player, randomChoice)
    return board


def repeat(n):
    global strategic
    board = create_board()
    player1 = 1
    player2 = 2
    m = 0
    if strategic:
        board = place(board, player1, [1, 1])
    else:
        board = random_place(board, player1)
    while m < n:
        board = random_place(board, player2)
        board = random_place(board, player1)
        m += 1
    return board


def row_win(board, player):
    if np.any(np.all(board == player, axis=1)):
        return True
    else:
        return False


def col_win(board, player):
    if np.any(np.all(board == player, axis=0)):
        return True
    else:
        return False


def diag_win(board, player):
    if np.all(np.diag(board) == player) or np.all(np.diag(np.fliplr(board)) == player):
        return True
    else:
        return False


def evaluate(board):
    winner = 0
    for player in [1, 2]:
        if row_win(board, player) or col_win(board, player) or diag_win(board, player):
            winner = player
    if np.all(board != 0) and winner == 0:
        winner = -1
    return winner


def play_game():
    n = 4
    try:
        board = repeat(n)
    except NameError:
        global strategic
        strategic = False
        board = repeat(n)
    winner = evaluate(board)
    return winner


def play_game_1000():
    results = []
    num_games = 1000
    for m in range(0, num_games):
        winner = play_game()
        results.append(winner)
    return results


def play_strategic_game():
    global strategic
    strategic = True
    results = play_game()
    return results


def play_strategic_game_1000():
    play_strategic_game()
    results = play_game_1000()
    return results


if __name__ == '__main__':
    '''
    Worked with Ronald Law
    '''

    game_results = play_game_1000()
    # print(f"The winner is player: {game_results}")

