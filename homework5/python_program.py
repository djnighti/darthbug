# A15736154

# import all modules here if you need any
import numpy as np
import matplotlib.pyplot as plt


def computeGridSukharev(n):
    d = 2 # 2D grid
    k = n**(1/2)
    # dispersion = 1 / (2 * (n**(1/d)))
    dispersion = 1 / (2 * (n**(1/d)-1))
    X = []
    Y = []
    odd_nums = range(0, n+1, 2)
    print(odd_nums)
    for i in odd_nums:
        for j in odd_nums:
            X.append(j*dispersion)
            Y.append(i*dispersion)
    return X, Y


def computeGridRandom(n):
    np.random.seed(1)
    X = np.random.random_sample(n)
    Y = np.random.random_sample(n)
    return X, Y


def computeHaltonSequence(n, p):
    S = list(np.zeros(n))
    for i in range(1, n+1):
        i_tmp = i
        f = 1/p
        while i_tmp > 0:
            q = i_tmp // p
            r = i_tmp % p
            S[i-1] = S[i-1] + f * r
            i_tmp = q
            f = f/p
    return S


def computeGridHalton(n, b1, b2):
    S1 = computeHaltonSequence(n, b1)
    S2 = computeHaltonSequence(n, b2)

    X = S1
    Y = S2

    return X, Y


if __name__ == '__main__':
    sukharev_uniform_option = "Sukharev Uniform Grid"
    sukharev_random_option = "Sukharev Random Grid"
    halton_option = "Halton Grid"
    all_options = 1
    my_option = sukharev_uniform_option
    n = 36

    if my_option == sukharev_uniform_option:
        X, Y = computeGridSukharev(n)
    elif my_option == sukharev_random_option:
        X, Y = computeGridRandom(n)
    elif my_option == halton_option:
        b1 = 2
        b2 = 3
        X, Y = computeGridHalton(n, b1, b2)
    elif my_option == all_options:
        X1, Y1 = computeGridSukharev(n)
        X2, Y2 = computeGridRandom(n)
        b1 = 2
        b2 = 3
        X3, Y3 = computeGridHalton(n, b1, b2)

        grid_space = 1 / (n** (1 / 2) -1)
        plt.figure(1)
        plt.scatter(X1, Y1)
        plt.axis([0, 1, 0, 1])
        plt.xticks(np.arange(0, 1, grid_space))
        plt.yticks(np.arange(0, 1, grid_space))
        plt.title(f'{str(sukharev_uniform_option)}')
        plt.grid(color='k', linestyle='--', linewidth=1)

        grid_space = 1 / (n ** (1 / 2))
        plt.figure(2)
        plt.scatter(X2, Y2)
        plt.axis([0, 1, 0, 1])
        plt.xticks(np.arange(0, 1, grid_space))
        plt.yticks(np.arange(0, 1, grid_space))
        plt.title(f'{str(sukharev_random_option)}')
        plt.grid(color='k', linestyle='--', linewidth=1)

        plt.figure(3)
        plt.scatter(X3, Y3)
        plt.axis([0, 1, 0, 1])
        plt.xticks(np.arange(0, 1, grid_space))
        plt.yticks(np.arange(0, 1, grid_space))
        plt.title(f'{str(halton_option)}')
        plt.grid(color='k', linestyle='--', linewidth=1)
    try:
        plt.scatter(X, Y)
        grid_space = 1 / (n** (1 / 2) -1)
        plt.xticks(np.arange(0, 1, grid_space))
        plt.yticks(np.arange(0, 1, grid_space))
        plt.title(f'{str(my_option)}')
        plt.grid(color='k', linestyle='--', linewidth=1)
        plt.axis([0, 1, 0, 1])
    except NameError:
        pass
    plt.show()




