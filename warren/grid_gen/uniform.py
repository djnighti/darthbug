import numpy as np
import matplotlib.pyplot as plt
from poolygon_simple import inside_obstacle

def computeGridSukharevCorner(n):
    x_fac = 10
    y_fac = 10
    d = 2  # 2D grid
    k = n**(1/d)-1
    dispersion = 1 / (2 * k)
    X = []
    Y = []
    odd_nums = range(0, n+1, 2)
    print(odd_nums)
    for i in odd_nums:
        for j in odd_nums:
            X.append(j*dispersion)
            Y.append(i*dispersion)
    return X, Y


def computeGridSukharevCenter(n):
    x_fac = 10
    y_fac = 5
    d = 2 # 2D grid
    k = n**(1/d)
    dispersion = 1 / (2 * k)
    X = []
    Y = []
    XY_list = []
    odd_nums = range(1, 2*(int(k)), 2)
    # print(obstacle_list)
    for i in odd_nums:
        for j in odd_nums:
            X_ij = j*dispersion*x_fac
            Y_ij = i*dispersion*y_fac
            X.append(X_ij)
            Y.append(Y_ij)
            XY_list.append([X_ij, Y_ij])
    return X, Y, XY_list


def computeGridRandom(n):
    np.random.seed(1)
    X = np.random.random_sample(n)
    Y = np.random.random_sample(n)
    return X, Y


def computeHaltonSequence(n, p):
    S = list(np.zeros(n))
    for i in range(1, n+1):
        i_tmp = i
        f = 1/p
        while i_tmp > 0:
            q = i_tmp // p
            r = i_tmp % p
            S[i-1] = S[i-1] + f * r
            i_tmp = q
            f = f/p
    return S


def computeGridHalton(n, b1, b2):
    S1 = computeHaltonSequence(n, b1)
    S2 = computeHaltonSequence(n, b2)
    X = S1
    Y = S2
    return X, Y


def computeOccupancyGrid(grid_list, obstacle_list):
    occupancy_point_list = []
    occupancy_index_list = []
    # print("grid list length: ", len(grid_list))
    result = False
    # for point in range(0, len(grid_list)):
    for point in grid_list:
        result = pointInsidePolygon(point, obstacle_list)
        if not result:
            occupancy_point_list.append(point)
            occupancy_index_list.append(0)
        else:
            occupancy_index_list.append(1)
    return occupancy_point_list, occupancy_index_list


def pointInsidePolygon(point, polygon_list):
    for obstacle in polygon_list:
        # print("my point: ",point)
        if inside_obstacle(point, obstacle):
            result = True
            return result
        else:
            result = False
    return result


if __name__ == '__main__':
    # Scale 1:100 (inches)
    O1 = [[0, 0], [0, 5], [1.915, 5], [1.915, 4.08], [1.3917, 3.863], [1.175, 3.34], [1.175, 1.76], [1.421, 1.166],
          [2.015, 0.92], [2.015, 0]]
    # split O1 (concave polygon) into several convex polygons
    O1_1 = [[0, 0], [1.421, 1.166], [2.015, 0.92], [2.015, 0]]
    O1_2 = [[0, 0], [0, 1.76], [1.175, 1.76], [1.421, 1.166]]
    O1_3 = [[0, 1.76], [0, 3.34], [1.175, 3.34], [1.175, 1.76]]
    O1_4 = [[0, 3.34], [0, 5], [1.3917, 3.863], [1.175, 3.34]]
    O1_5 = [[1.3917, 3.863], [0, 5], [1.915, 5], [1.915, 4.08]]

    O2 = [[1.915, 4.08], [1.915, 5], [7.245, 5], [7.245, 4.08]]
    O3 = [[2.015, 1.42], [1.675, 1.76], [1.675, 3.34], [1.915, 3.58], [4.895, 3.58], [4.859, 2.74]]
    O4 = [[2.015, 0], [2.015, 0.92], [4.895, 2.24], [7.245, 0.92], [7.245, 0]]
    O5 = [[4.859, 2.74], [4.895, 3.58], [7.245, 3.58], [8.079, 3.334], [8.425, 2.5], [8.079, 1.6656], [7.245, 1.32]]
    O6 = [[7.245, 0], [7.245, 0.92], [8.36, 1.383], [8.825, 2.5], [8.36, 3.617], [7.245, 4.08], [7.245, 5], [10, 5], [10, 0]]

    # split O6 (concave polygon) into several convex polygons
    O6_1 = [[7.245, 0], [7.245, 0.92], [8.36, 1.383], [10, 0]]
    O6_2 = [[10, 0], [8.36, 1.383], [8.825, 2.5], [10, 2.5]]
    O6_3 = [[8.825, 2.5], [8.36, 3.617], [10, 5], [10, 2.5]]
    O6_4 = [[8.36, 3.617], [7.245, 4.08], [7.245, 5], [10, 5]]
    # my_obstacle_list = [O2, O4]
    my_obstacle_list = [O1_1, O1_2, O1_3, O1_4, O1_5, O2, O3, O4, O5, O6_1, O6_2, O6_3, O6_4]
    # my_obstacle_list = [O1_1, O1_2, O1_3, O1_4, O1_5, O2, O3, O4, O5,O6_1, O6_2,O6_3]
    # for obstacle in my_obstacle_list:
    #     obstacle_draw = plt.Polygon(obstacle, fc="g")
    #     plt.gca().add_patch(obstacle_draw)

    sukharev_uniform_center_option = "Sukharev Uniform Center Grid"
    sukharev_uniform_corner_option = "Sukharev Uniform Corner Grid"
    sukharev_random_option = "Sukharev Random Grid"
    halton_option = "Halton Grid"
    grid_option = "center"
    all_options = 1
    my_option = sukharev_uniform_center_option
    n = 900
    if grid_option == "corner":
        grid_space = 1 / (n ** (1 / 2) - 1)
    elif grid_option == "center":
        grid_space = 1 / (n ** (1 / 2))
    xmin = 0
    ymin = 0
    xmax = 10
    ymax = 5
    x_fac = 10
    y_fac = 5

    if my_option == sukharev_uniform_center_option:
        X, Y, XY_list = computeGridSukharevCenter(n)
        my_occ_point_list, my_occ_index_list = computeOccupancyGrid(XY_list, my_obstacle_list)
        my_occ_index_np = np.asarray(my_occ_index_list)
        my_occ_index_np = np.reshape(my_occ_index_np, (int(1/grid_space), (int(1/grid_space))))
        # my_occ_index_np = np.reshape(my_occ_index_np, (10, 10))
        my_occ_index = my_occ_index_np.tolist()
        print("reshaped index list", my_occ_index)

    if my_option == sukharev_uniform_corner_option:
        X, Y = computeGridSukharevCorner(n)
    elif my_option == sukharev_random_option:
        X, Y = computeGridRandom(n)
    elif my_option == halton_option:
        b1 = 2
        b2 = 3
        X, Y = computeGridHalton(n, b1, b2)
    elif my_option == all_options:
        X1, Y1 = computeGridSukharev(n,grid_option)
        X2, Y2 = computeGridRandom(n)
        b1 = 2
        b2 = 3
        X3, Y3 = computeGridHalton(n, b1, b2)

        plt.figure(1)
        plt.scatter(X1, Y1)
        plt.axis([xmin, xmax, ymin, ymax])
        plt.xticks(np.arange(xmin, xmax, grid_space*x_fac))
        plt.yticks(np.arange(ymin, ymax, grid_space*y_fac))
        plt.title(f'{str(sukharev_uniform_option)} with a {grid_option} grid')
        plt.grid(color='k', linestyle='--', linewidth=1)

        plt.figure(2)
        plt.scatter(X2, Y2)
        plt.axis([xmin, xmax, ymin, ymax])
        plt.xticks(np.arange(xmin, xmax, grid_space))
        plt.yticks(np.arange(ymin, ymax, grid_space))
        plt.title(f'{str(sukharev_random_option)}')
        plt.grid(color='k', linestyle='--', linewidth=1)

        plt.figure(3)
        plt.scatter(X3, Y3)
        plt.axis([xmin, xmax, ymin, ymax])
        plt.xticks(np.arange(xmin, xmax, grid_space))
        plt.yticks(np.arange(ymin, ymax, grid_space))
        plt.title(f'{str(halton_option)}')
        plt.grid(color='k', linestyle='--', linewidth=1)
    try:
        print(my_occ_point_list)
        print("original list length: ",len(my_occ_point_list))
        print("collision-less list length: ",len(my_occ_point_list))
        for point in my_occ_point_list:
            plt.scatter(point[0], point[1])
        plt.xticks(np.arange(xmin, xmax, grid_space * x_fac))
        plt.yticks(np.arange(ymin, ymax, grid_space * y_fac))
        plt.title(f'{str(my_option)} with a {grid_option} grid')
        plt.grid(color='k', linestyle='--', linewidth=1)
        plt.axis([xmin, xmax, ymin, ymax])
    except NameError:
        pass
    plt.show()





