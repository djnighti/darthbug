#!/usr/bin/env python3
# A15736154 #PID

# import all modules here if you need any
import matplotlib.pyplot as plt
import numpy as np
import math
from auxiliary_functions import inCollision, isPointInConvexPolygon, dist
from poolygon_simple import inside_obstacle


class Node:
    """
    Node class which has the x and y position along with the parent
    """

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.parent = None


class RRT:

    def __init__(self, start, goal, obstacle_list):
        (xs, ys) = start
        (xg, yg) = goal
        self.start = start
        self.goal = goal
        self.x = []
        self.y = []
        self.parent = []
        self.x.append(xs)
        self.y.append(ys)
        self.parent.append(0)
        self.obstacles = obstacle_list
        self.node_list = []
        # self.node_list.append(Node(xs, ys))
        self.goalFlag = False
        self.goalState = None
        self.path = []
        self.nodePath = []
        self.alpha = 0.75
        self.no_restart = False
        self.iteration = 0

    def add_node(self, n, x, y):
        self.x.insert(n, x)
        self.y.insert(n, y)
        self.node_list.append(Node(x, y))

    def remove_node(self, n):
        self.x.pop(n)
        self.y.pop(n)

    def sample_envir(self):
        qx_random = np.random.sample() * 10.1
        qy_random = np.random.sample() * 5.1
        q_random = [qx_random, qy_random]
        return q_random

    def add_edge(self, child, parent):
        self.parent.insert(child, parent)

    def remove_edge(self, n):
        self.parent.pop(n)

    def number_of_nodes(self):
        n = len(self.x)
        return n

    def calcDistNodeToPoint(self, n1, n2):
        try:
            (x1, y1) = (self.x[n1], self.y[n1])
            (x2, y2) = (self.x[n2], self.y[n2])
        except TypeError:
            (x1, y1) = (n1.x, n1.y)
            (x2, y2) = (n2[0], n2[1])
        d = math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
        return d

    def getNearestNode(self, n):
        n_near = 0
        close_node = self.node_list[n_near]
        if type(n) == int:
            dmin = self.calcDistNodeToPoint(0, n)
            for i in range(0, n):
                if self.calcDistNodeToPoint(i, n) < dmin:
                    dmin = self.calcDistNodeToPoint(i, n)
                    n_near = i
        elif type(n) == list:
            print("in list")
            first_node_obj = self.node_list[0]
            dmin = self.calcDistNodeToPoint(first_node_obj, n)
            last_node_obj = self.node_list[-1]
            last_node = [last_node_obj.x, last_node_obj.y]
            for i in self.node_list:
                if self.calcDistNodeToPoint(i, last_node) < dmin:
                    dmin = self.calcDistNodeToPoint(i, n)
                    close_node = i
                    n_near += 1
            n_near = n_near - 1
        else:
            print("Wrong input was entered")
        return n_near, close_node

    def inObstacle(self, n):
        [x, y] = (self.x[n], self.y[n])
        obstacles_copy = self.obstacles.copy()
        for obstacle in obstacles_copy:
            result = inside_obstacle([x, y], obstacle)
        return result

    def crossObstacle(self, n1, n2):
        [x1, y1] = (self.x[n1], self.y[n1])
        [x2, y2] = (self.x[n2], self.y[n2])
        obstacles_copy = self.obstacles.copy()
        result = inCollision([x1, y1], [x2, y2], obstacles_copy)
        return result

    def step(self, n_near, n_rand, dmax=0.25):
        d = self.calcDistNodeToPoint(n_near, n_rand)
        n_new = n_rand
        new_node_found = False
        while not new_node_found:
            try:
                if d > dmax:
                    (x_near, y_near) = (self.x[n_near], self.y[n_near])
                    (x_rand, y_rand) = (self.x[n_rand], self.y[n_rand])
                    (x_new, y_new) = (x_near * self.alpha + (1 - self.alpha) * x_rand), (y_near * self.alpha + (1 - self.alpha) * y_rand)
                    # (x_slope, y_slope) = (x_rand - x_near, y_rand - y_near)
                    # theta = math.atan2(y_slope, x_slope)
                    # (x_new, y_new) = ((x_near + dmax * math.cos(theta)), (y_near + dmax * math.sin(theta)))
                else:
                    (x_rand, y_rand) = (self.x[n_rand], self.y[n_rand])
                    (x_new, y_new) = (x_rand, y_rand)
                self.remove_node(n_rand)
                d_check = dist([x_new, y_new], [self.goal[0], self.goal[1]])
                # print("d_check", d_check)
                if abs(x_new - self.goal[0]) < dmax and abs(y_new - self.goal[1]) < dmax:
                    self.add_node(n_new, self.goal[0], self.goal[1])
                    self.add_edge(n_new, n_near)
                    self.goalFlag = True
                    self.goalState = n_rand
                    new_node_found = True
                    # print("new_node_found because goal", new_node_found)
                else:
                    self.add_node(n_rand, x_new, y_new)
                    cross_Obstacle_result = self.crossObstacle(n_near, n_new)
                    in_Obstacle_result = self.inObstacle(n_new)
                    # print("coordinates",[self.x[n_near], self.y[n_near]])
                    # print("[cross_Obstacle_result, in_Obstacle_result]",[cross_Obstacle_result, in_Obstacle_result])
                    if not cross_Obstacle_result and not in_Obstacle_result:
                        self.add_edge(n_new, n_near)
                        new_node_found = True
                        # print("new_node_found because no obstacle", new_node_found)
                        # print("new_node_found because no obstacle", self.x)
                    else:
                        self.remove_node(n_new)
                        new_node_found = False
                        # print("no new_node_found because obstacle", new_node_found)
                        # print("no new_node_found because obstacle", self.x)
                # print(new_node_found)
            except ZeroDivisionError:
                d_check = "zero"
        return d_check

    def bias(self, ngoal):
        n = self.number_of_nodes()
        self.add_node(n, ngoal[0], ngoal[1])
        q_nearest, dummy_variable = self.getNearestNode(n)
        d_check = self.step(q_nearest, n)
        # self.connect(q_nearest, n)
        return self.x, self.y, self.parent, d_check

    def expand(self):
        n = self.number_of_nodes()
        (x, y) = self.sample_envir()
        self.add_node(n, x, y)
        # if not self.inObstacle(n):
        q_nearest, dummy_variable = self.getNearestNode(n)
        d_check = self.step(q_nearest, n)
            # self.connect(q_nearest, n)
            # print("expand: not in obstacle")
        # else:
        #     self.remove_node(n)
        #     # print("expand: in obstacle")
        #     d_check = "in obstacle"
        return self.x, self.y, self.parent, d_check

    def pathToGoal(self):
        if self.goalFlag:
            self.path = []
            self.path.append(self.goalState)
            newPos = self.parent[self.goalState]
            while newPos != 0:
                self.path.append(newPos)
                newPos = self.parent[newPos]
                self.nodePath.append(newPos)
            self.path.append(0)
            # try:
            #     newPos = self.parent[self.goalState]
            #     while newPos != 0:
            #         self.path.append(newPos)
            #         newPos = self.parent[newPos]
            #         self.nodePath.append(newPos)
            #     self.path.append(0)
            # except IndexError:
            #     print("restart sim")
            #     self.no_restart = True
            #     plt.clf()
            #     % reset - f
        return self.goalFlag

    def getPathCoordinates(self):
        x_pathCoordinates = []
        y_pathCoordinates = []
        xy_coordinates = []
        self.pathToGoal()
        for node in self.path:
            x = self.x[node]
            y = self.y[node]
            x_pathCoordinates.append(x)
            y_pathCoordinates.append(y)
            xy_coordinates.append([x, y])
        return x_pathCoordinates, y_pathCoordinates, xy_coordinates

    def drawGraph(self):
        plt.clf()
        for obstacle in self.obstacles:
            obstacle_draw = plt.Polygon(obstacle, fc="g")
            plt.gca().add_patch(obstacle_draw)
        for point in self.parent:
            X_tree = [self.x[point], self.x[self.parent[point]]]
            Y_tree = [self.y[point], self.y[self.parent[point]]]
            plt.plot(X_tree, Y_tree, "b")
            plt.scatter(X_tree, Y_tree, color="b")
        X_path, Y_path, XY_path = self.getPathCoordinates()
        plt.plot(self.start[0], self.start[1], "xr")
        plt.plot(self.goal[0], self.goal[1], "xr")
        plt.axis([0, 10, 0, 5])
        plt.grid(True)
        plt.pause(0.01)
        plt.plot(X_path, Y_path, 'r')
        plt.scatter(X_path, Y_path, color='r')

    def planning(self, animation, goal_check, alpha):
        self.alpha = alpha
        iteration = 0
        while iteration < 1000:
            while not self.goalFlag:
                if animation:
                    try:
                        plt.clf()
                        if iteration % int(1/goal_check) == 0:
                            X, Y, Parent, d_check = self.bias(self.goal)
                        else:
                            X, Y, Parent, d_check = self.expand()
                        for obstacle in self.obstacles:
                            obstacle_draw = plt.Polygon(obstacle, fc="g")
                            plt.gca().add_patch(obstacle_draw)
                        for point in self.parent:
                            X_tree = [X[point], X[Parent[point]]]
                            Y_tree = [Y[point], Y[Parent[point]]]
                            plt.plot(X_tree, Y_tree, "b")
                            plt.scatter(X_tree, Y_tree, color="b")
                        plt.plot(self.start[0], self.start[1], "xr")
                        plt.plot(self.goal[0], self.goal[1], "xr")
                        plt.axis([0, 10, 0, 5])
                        plt.grid(True)
                        # plt.show()
                        plt.pause(0.0001)
                        # iteration = len(self.x)

                    except IndexError:
                        # print("Index Error from planning")
                        pass
                print("iterations: ", iteration, self.number_of_nodes())
                # iteration = self.number_of_nodes()
                iteration +=1
                if iteration > 1000:
                    print("max iterations")
                    return False

            iteration = 1000
        print("goal state", self.goalFlag)
        X_path, Y_path, XY_path = self.getPathCoordinates()
        XY_path.reverse()
        plt.plot(X_path, Y_path, 'r')
        plt.scatter(X_path, Y_path, color='r')
        plt.show()
        print("goal reached, path: ", XY_path)
        XY_path = True
        return XY_path


if __name__ == '__main__':
    '''
        You need to complete this planning part of the code, note that it has a
        random sampling part that outputs a random_point, but still you need
        to try to connect this random_point to the tree, by finding the
        closest node, and verify that the segment connecting to the tree
        is collision free. To check for collisions use an imported auxiliary
        function.

        Task 1: Find the nearest node to the sampled point

        Task 2: Find the distance to that node and if distance is greater than 
                0.25 use logic mentioned in the question to find the new point.

        Task 3: Check if the new point connects to the tree you are building
                without hitting an obstacle. You can use the auxiliary function
                to do this.

        Task 4: If collision free from the tree to the new point. Make this
                point a node in the tree with its parent as the nearest node
                to the tree.

        You can define any other methods in for the rrt object (i.e. functions
        that apply to the rrt object attributes) and which can help you
        complete the assignment, for example to obtain q nearby
    '''

    # Define obstacle polygon in the counter clockwise direction
    O1_ = []
    my_obstacle_list = [[[3, 3], [4, 3], [4, 4], [3, 4]], [[8, 8], [7, 6], [9, 9]]]
    O1 = [[0, 0], [0, 5], [1.915, 5], [1.915, 4.08], [1.3917, 3.863], [1.175, 3.34], [1.175, 1.76], [1.421, 1.166],[2.015, 0.92], [2.015, 0]]
    O1_1 = [[0, 0],[1.421, 1.166],[2.015, 0.92], [2.015, 0]]
    O1_2 = [[0, 0],[0, 1.76],[1.175, 1.76], [1.421, 1.166]]
    O1_3 = [[0, 1.76],[0,3.34],[1.175, 3.34], [1.175, 1.76]]
    O1_4 = [[0,3.34],[0, 5],[1.3917, 3.863], [1.175, 3.34]]
    O1_5 = [[1.3917, 3.863], [0, 5], [1.915, 5], [1.915, 4.08]]

    O2 = [[1.915, 4.08], [1.915, 5], [7.245, 5], [7.245, 4.08]]
    O3 = [[2.015, 1.42], [1.675, 1.76], [1.675, 3.34], [1.915, 3.58], [4.895, 3.58], [4.859, 2.74]]
    O4 = [[2.015, 0], [2.015, 0.92], [4.895, 2.24], [7.245, 0.92], [7.245, 0]]
    O5 = [[4.859, 2.74], [4.895, 3.58], [7.245, 3.58], [8.079, 3.334], [8.425, 2.5], [8.079, 1.6656], [7.245, 1.32]]
    O6 = [[7.245, 0], [7.245, 0.92], [8.36, 1.383], [8.825, 2.5], [8.36, 3.617], [7.245, 4.08], [7.245, 5], [10, 5],[10, 0]]

    O6_1 = [[7.245, 0], [7.245, 0.92], [8.36, 1.383], [10, 0]]
    O6_2 = [[10, 0], [8.36, 1.383], [8.825, 2.5], [10, 2.5]]
    O6_3 = [[8.825, 2.5], [8.36, 3.617], [10, 5], [10, 2.5]]
    O6_4 = [[8.36, 3.617], [7.245, 4.08], [7.245, 5], [10, 5],[10, 0]]

    my_obstacle_list = [O1_1, O1_2, O1_3, O1_4, O1_5, O2,O3,O4,O5,O6_1, O6_2, O6_3, O6_4]
    # my_obstacle_list = [O2,O3,O4,O5]
    # Set Initial parameters
    rrt = RRT(start=[2, 1.2], goal=[6.5, 4], obstacle_list=my_obstacle_list)

    show_animation = True
    check_goal = 0.1
    my_alpha = 0.75
    path = rrt.planning(show_animation, check_goal, my_alpha)
    # print("my path", path)
    # Draw final path
    # node_list = [Node(0, 0), Node(1, 0), Node(1, 1)]

    # # testing from gradescope
    # show_animation = False
    # rrt2 = RRT(start=[2, 2], goal=[5, 5], obstacle_list=my_obstacle_list)
    # rrt2.node_list.append(Node(0, 0))
    # rrt2.node_list.append(Node(1, 0))
    # rrt2.node_list.append(Node(1, 1))
    # print("node list: ", rrt2.node_list)
    # print("node list length: ", len(rrt2.node_list))
    # point = [2, 2]
    # index, node = rrt2.getNearestNode(point)
    # print(index, [node.x, node.y])
    # # path = rrt2.planning(show_animation, check_goal, my_alpha)
    # if show_animation:
    #     # rrt.drawGraph()
    #     plt.grid(True)
    #     plt.show()

    '''

                        # if self.inObstacle():
                        #     self.remove_node(n)
                        # if self.crossObstacle(n - 1, n):
                        #     self.remove_node(n)
                        # else:
                        #     self.add_edge(n - 1, n)'''

    '''

        iteration = 0
        max_iterations = 1000
        while iteration < max_iterations:
            while not self.goalFlag:
                random_number = np.random.rand()
                if random_number > goal_check:
                    self.expand()
                    # X, Y, Parent, d_check = self.expand()
                    # print("bias: ", d_check)
                else:
                    self.bias(self.goal)
                    # X, Y, Parent, d_check = self.bias(self.goal)
                    # print("expand: ", d_check)
                iteration = iteration + 1
                if iteration > max_iterations:
                    result = "max iterations"
                    print(result)
                    return result
            if animation:
                self.drawGraph()
            X_path, Y_path, XY_path = self.getPathCoordinates()
            XY_path.reverse()
            print("iteration : ", iteration)
            iteration = max_iterations
        print("Found goal?: ", self.goalFlag)
        return XY_path'''