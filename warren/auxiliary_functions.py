#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import math
import copy
import numpy as np
# from python_program import Node
from poolygon_simple import inside_obstacle, computeDistancePointToPolygon
"""
@author: sonia

This function samples a segment in the plane according to
a Halton sequence by sampling over [0, 1] and then using
the parametrization of the segment to map to the
corresponding point in the segment.

The function can be extended to any parametrized curve.
"""


def haltonPointInSegment(n, base, q1, q2):
    # parameters:
    # n = the nth point in the Halton sequence
    # base = the prime number basis of the Halton sequence
    # q1 = first end point of segment, entered as a list
    # q2 = second end point of segment, entered as a list

    # convert to np arrays
    n = int(n)
    base = float(base)

    # nth halton sequence point on [0, 1]
    pB = 0.
    f = 1/base
    itmp = n
    x = 0.
    while(itmp > 0):
        q = math.floor(itmp/base)
        r = itmp % base
        x = x + f * r
        itmp = q
        f = f/base
        pB = x

    # Obtain point in segment
    q = (pB*q1[0] + (1 - pB)*q2[0], pB*q1[1] + (1 - pB)*q2[1])
    return q

def dist(p1, p2):
    return np.sqrt(np.square(p1[0] - p2[0]) + np.square(p1[1] - p2[1]))

def isPointInConvexPolygon(q, P):
    """
    :param q: a point
    :param P: a list of points to define a polygon
    :return: False if point is outside of polygon, True if point is on or inside
    the polygon
    """
    polyPList = copy.deepcopy(P)  # this ensures the changes stay local

    polyPList.append(polyPList[0])
    # print("polylist test: ", polyPList)
    # Initialize relevant variables
    pV = [0, 0]
    fail = 0
    qV = [0, 0]
    pPHV = [0, 0]
    passVar = 0

    for i in range(len(polyPList) - 1):  # If the point is a vertex, autopass
        if q == polyPList[i]:
            passVar = 1
            fail = 1
            # print("vertex")
    if passVar == 0:
        for j in range(len(polyPList) - 1):
            p1 = polyPList[j]
            p2 = polyPList[j + 1]
            # create vector along segment
            pPHV[0] = (p2[0] - p1[0]) / dist(p1, p2)
            pPHV[1] = (p2[1] - p1[1]) / dist(p1, p2)
            # rotate vector 90deg so it is normal to segment
            pV[0] = pPHV[1] * -1
            pV[1] = pPHV[0]
            # create vector from q point to first segment point
            try:
                # print([dist(q, p1)])
                qV[0] = (q[0] - p1[0]) / dist(q, p1)
                qV[1] = (q[1] - p1[1]) / dist(q, p1)
            except RuntimeWarning:
                pass
                # print([dist(q, p1)])
            # take dot product and if it is negative then q is outside
            # respective plane
            dotP = np.dot(pV, qV)
            if dotP < 0:
                fail = 1

    del polyPList  # make sure variable is not reused

    if fail:
        return False
    else:
        return True

def inCollision(nearest_node, new_node, obstacleList):
    if type(nearest_node) != list:
        input_conversion = [nearest_node.x, nearest_node.y]
        nearest_node = input_conversion
    # feel free to try different values on n of collisions to improve your collision checker
    # But make sure that you do not alternate these values for submission files
    collision_points_on_segment = 500
    prime_number = 17
    dmin = 0.5
    nearest_node = (nearest_node[0], nearest_node[1])
    new_node = (new_node[0], new_node[1])
    for obstacle in obstacleList:
        val1 = inside_obstacle(new_node, obstacle)
        if val1:
            result = True
            # print("if",[val1, new_node])
            return result
    else:
        # print("else",[val1, new_node])
        pass
    for i in range(1, collision_points_on_segment):
        point = haltonPointInSegment(i, prime_number, nearest_node, new_node)
        for obstacle in obstacleList:
            val2 = inside_obstacle(point, obstacle)
            # distance_to_poly = computeDistancePointToPolygon(point, obstacle)
            # print([val1, new_node])
            if val2:
                result = True # collision
                # print("inside obstacle: ", result)
            else:
                result = False  # no collision
                # print("not inside obstacle: ", result)
                # if distance_to_poly < dmin:
                #     result = True # collision
                # else:
                #     result = False  # no collision
    # print("inside obstacle?", result)
    return result


if __name__ == '__main__':
    # check halton point
    # point = haltonPointInSegment(2, 2, (0, 0), (1, 1))
    # # check collision function
    # obstacle = [[3, 3], [4, 3], [4, 4], [3, 4]]
    # q = (3.5, 3.5)
    # val = isPointInConvexPolygon(q, obstacle)
    O1 = [[0, 0], [0, 5], [1.915, 5], [1.915, 4.08], [1.3917, 3.863], [1.175, 3.34], [1.175, 1.76], [1.421, 1.166],
          [2.015, 0.92], [2.015, 0]]

    new_o1 = []
    print(O1)
    for n in range(0,10,2):
        if n % 2 ==0:
            new_o1.append(O1[n])
    print(new_o1)

