#!/usr/bin/env python3
# A15736154 #PID

# import all modules here if you need any
import matplotlib.pyplot as plt
import numpy as np
import math
from auxiliary_functions import inCollision, isPointInConvexPolygon, dist


class Node:
    """
    Node class which has the x and y position along with the parent
    """

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.parent = None


class RRT:

    def __init__(self, start, goal, obstacle_list):
        (xs, ys) = start
        (x2, yg) = goal
        self.start = start
        self.goal = goal
        self.x = []
        self.y = []
        self.parent = []
        self.x.append(xs)
        self.y.append(ys)
        self.parent.append(0)
        self.obstacles = obstacle_list
        self.node_list = []
        self.node_list.append([])
        self.goalFlag = False
        self.goalState = None
        self.path = []
        self.nodePath = []
        self.alpha = 0.75

    def add_node(self, n, x, y):
        self.x.insert(n, x)
        self.y.insert(n, y)

    def remove_node(self, n):
        self.x.pop(n)
        self.y.pop(n)

    def sample_envir(self):
        qx_random = np.random.sample() * 10.1
        qy_random = np.random.sample() * 5.1
        q_random = [qx_random, qy_random]
        return q_random

    def add_edge(self, parent, child):
        self.parent.insert(child, parent)

    def remove_edge(self, n):
        self.parent.pop(n)

    def connect(self, n1, n2):
        if self.crossObstacle(n1, n2):
            self.remove_node(n2)
            return False
        else:
            # print("adding to parent list", self.parent, len(self.parent))
            # print([len(self.x),len(self.y), len(self.parent)])
            self.add_edge(n1, n2)
            return True

    def number_of_nodes(self):
        n = len(self.x)
        return n

    def calcDistNodeToPoint(self, n1, n2):
        (x1, y1) = (self.x[n1], self.y[n1])
        (x2, y2) = (self.x[n2], self.y[n2])
        d = math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
        return d

    def getNearestNode(self, n):
        dmin = self.calcDistNodeToPoint(0, n)
        n_near = 0
        for i in range(0, n):
            if self.calcDistNodeToPoint(i, n) < dmin:
                dmin = self.calcDistNodeToPoint(i, n)
                n_near = i
        return n_near

    def inObstacle(self, n):
        [x, y] = (self.x[n], self.y[n])
        obstacles_copy = self.obstacles.copy()
        for obstacle in range(0, len(obstacles_copy)):
            # print(obstacle)
            check_collision = isPointInConvexPolygon([x, y], obstacles_copy[obstacle])
        return check_collision

    def crossObstacle(self, n1, n2):
        [x1, y1] = (self.x[n1], self.y[n1])
        [x2, y2] = (self.x[n2], self.y[n2])
        obstacles_copy = self.obstacles.copy()
        check_collision = inCollision([x1, y1], [x2, y2], obstacles_copy)
        # print("collision check: ", check_collision)
        return check_collision

    def step(self, n_near, n_rand, dmax=0.1):
        d = self.calcDistNodeToPoint(n_near, n_rand)
        try:
            if d > dmax:
                (x_near, y_near) = (self.x[n_near], self.y[n_near])
                (x_rand, y_rand) = (self.x[n_rand], self.y[n_rand])
                (x_slope, y_slope) = (x_rand - x_near, y_rand - y_near)
                theta = math.atan2(y_slope, x_slope)
                (x_new, y_new) = ((x_near + dmax * math.cos(theta)), (y_near + dmax * math.sin(theta)))
                self.remove_node(n_rand)
                d_check = dist([x_new, y_new], [self.goal[0], self.goal[1]])
                if abs(x_new - self.goal[0]) < dmax and abs(y_new - self.goal[1]) < dmax:
                    self.add_node(n_rand, self.goal[0], self.goal[1])
                    self.goalFlag = True
                    self.goalState = n_rand
                else:
                    self.add_node(n_rand, x_new, y_new)
                if self.inObstacle(n_rand):
                    self.remove_node(n_rand)
            else:
                d_check = "too large"
        except ZeroDivisionError:
            d_check = "zero"
        return d_check

    def bias(self, ngoal):
        n = self.number_of_nodes()
        self.add_node(n, ngoal[0], ngoal[1])

        for node in range(0, n):
            if not self.inObstacle(n):
                n_nearest = self.getNearestNode(node)
                d_check = self.step(n_nearest, node)
                self.connect(n_nearest, node)
            else:
                d_check = "in obstacle"
        return self.x, self.y, self.parent, d_check

    def expand(self):
        n = self.number_of_nodes()
        x, y = self.sample_envir()
        self.add_node(n, x, y)

        for node in range(0, n):
            if not self.inObstacle(n):
                n_nearest = self.getNearestNode(node)
                d_check = self.step(n_nearest, node)
                self.connect(n_nearest, node)
            else:
                d_check = "in obstacle"
        # if not self.inObstacle(n):
        #     n_near = self.getNearestNode(n)
        #     d_check = self.step(n_near, n)
        #     self.connect(n_near, n)
        # else:
        #     d_check = "in obstacle"
        return self.x, self.y, self.parent, d_check

    def pathToGoal(self):
        if self.goalFlag:
            self.path = []
            self.path.append(self.goalState)
            try:
                newPos = self.parent[self.goalState]
                while newPos != 0:
                    self.path.append(newPos)
                    newPos = self.parent[newPos]
                    self.nodePath.append(newPos)
                self.path.append(0)
            except IndexError:
                print("index error from path to goal")
        return self.goalFlag

    def getPathCoordinates(self):
        x_pathCoordinates = []
        y_pathCoordinates = []
        self.pathToGoal()
        print("node Path: ", self.nodePath)
        print("Path: ", self.path)
        print("goal state parent: ", self.parent[self.goalState])
        print("parent list: ", self.parent)
        print("parent length: ", len(self.parent))
        print("goal state: ", self.goalState)
        for node in self.path:
            x = self.x[node]
            y = self.y[node]
            x_pathCoordinates.append(x)
            y_pathCoordinates.append(y)
        return x_pathCoordinates, y_pathCoordinates

    def planning(self, animation=True):
        iteration = 0
        while iteration < 500:
            while not self.goalFlag:
                if animation:
                    try:
                        plt.clf()
                        if iteration % 10 == 0:
                            X, Y, Parent, d_check = self.bias(self.goal)
                            # print("bias: ", d_check)
                        else:
                            X, Y, Parent, d_check = self.expand()
                            # print("expand: ", d_check)
                        for obstacle in self.obstacles:
                            obstacle_draw = plt.Polygon(obstacle, fc="g")
                            plt.gca().add_patch(obstacle_draw)
                        for point in range(0, len(self.parent)):
                            # X_tree.append(X[-1])
                            # Y_tree.append(Y[-1])
                            # X_tree = ([X[-1], X[Parent[-1]]])
                            # Y_tree = ([Y[-1], Y[Parent[-1]]])
                            X_tree = [X[point], X[Parent[point]]]
                            Y_tree = [Y[point], Y[Parent[point]]]
                            plt.plot(X_tree, Y_tree, "b")
                            plt.scatter(X_tree, Y_tree, color="b")
                        plt.plot(self.start[0], self.start[1], "xr")
                        plt.plot(self.goal[0], self.goal[1], "xr")
                        plt.axis([0, 10, 0, 5])
                        plt.grid(True)
                        plt.pause(0.01)
                    except IndexError:
                        print("Index Error from planning")
                iteration = iteration + 1
                if iteration > 500:
                    print("max iterations")
                    return False
            iteration = 500
            X_path, Y_path = self.getPathCoordinates()
            print(X_path, Y_path)
            plt.plot(X_path, Y_path, 'r')
            plt.scatter(X_path, Y_path, color='r')
            plt.show()
            print("goal reached")
        return True

    def drawGraph(self):
        """ Draw graph
        edit this function at your own risk
        """
        plt.clf()
        # draw random point
        # plt.plot(self.sample_envir(), "^k")
        # print(self.x)
        # print("testing", self.parent[self.x[-1]])
        # plt.plot([self.x[-1], self.x[self.parent[-1]]], [self.y[-1], self.y[self.parent[-1]]])
        # x_data =[]
        # x_parent_data = []
        # y_data = []
        # y_parent_data = []
        # x_data.append(self.x[-1])
        # x_parent_data.append(self.x[self.parent[-1]])
        # y_data.append(self.y[-1])
        # y_parent_data.append(self.y[self.parent[-1]])
        # plt.plot([x_data, x_parent_data],[y_data, y_parent_data])
        # print(self.parent)
        # print([self.x, self.x])

        # plt.scatter([X, X[Parent]], [Y, Y[Parent]])
        # draw the obstacle
        for obstacle in self.obstacles:
            obstacle_draw = plt.Polygon(obstacle, fc="g")
            plt.gca().add_patch(obstacle_draw)
        # draw the start and goal points
        plt.plot(self.start[0], self.start[1], "xr")
        plt.plot(self.goal[0], self.goal[1], "xr")
        plt.axis([0, 10, 0, 5])
        plt.grid(True)
        plt.pause(0.01)


if __name__ == '__main__':
    '''
        You need to complete this planning part of the code, note that it has a
        random sampling part that outputs a random_point, but still you need
        to try to connect this random_point to the tree, by finding the
        closest node, and verify that the segment connecting to the tree
        is collision free. To check for collisions use an imported auxiliary
        function.

        Task 1: Find the nearest node to the sampled point

        Task 2: Find the distance to that node and if distance is greater than 
                0.25 use logic mentioned in the question to find the new point.

        Task 3: Check if the new point connects to the tree you are building
                without hitting an obstacle. You can use the auxiliary function
                to do this.

        Task 4: If collision free from the tree to the new point. Make this
                point a node in the tree with its parent as the nearest node
                to the tree.

        You can define any other methods in for the rrt object (i.e. functions
        that apply to the rrt object attributes) and which can help you
        complete the assignment, for example to obtain q nearby
    '''

    # Define obstacle polygon in the counter clockwise direction
    O1 = [[0, 0], [0, 5], [1.915, 5], [1.915, 4.08], [1.3917, 3.863], [1.175, 3.34], [1.175, 1.76], [1.421, 1.166],[2.015, 0.92], [2.015, 0]]
    O2 = [[1.915, 4.08], [1.915, 5], [7.245, 5], [7.245, 4.08]]
    O3 = [[2.015, 1.42], [1.675, 1.76], [1.675, 3.34], [1.915, 3.58], [4.895, 3.58], [4.859, 2.74]]
    O4 = [[2.015, 0], [2.015, 0.92], [4.895, 2.24], [7.245, 0.92], [7.245, 0]]
    # O5 = [[4.859,2.74], [4.895,3.58], [7.245,3.68], [8.0794,2.845], [8.825,2.5], [8.08,2.154], [7.245,1.32]]
    O5 = [[4.859, 2.74], [4.895, 3.58], [7.245, 3.68], [8.079, 3.334], [8.425, 2.5], [8.079, 1.6656], [7.245, 1.32]]
    O6 = [[7.245, 0], [7.245, 0.92], [8.36, 1.383], [8.825, 2.5], [8.36, 3.617], [7.245, 4.08], [7.245, 5], [10, 5],[10, 0]]
    my_obstacle_list = [O1, O2, O3, O4, O5, O6]
    # my_obstacle_list = [O1]
    # Set Initial parameters
    track_width = 0.2
    rrt = RRT(start=[O5[2][0], O5[2][1] + track_width], goal=[O4[2][0], O4[2][1] + track_width], obstacle_list=my_obstacle_list)
    print(rrt.goal)
    show_animation = True
    check_goal = 0.1
    my_alpha = 0.75
    path = rrt.planning(animation=show_animation)
    # print(rrt.x)
    # Draw final path
    # if show_animation:
    #     rrt.drawGraph()
    #     plt.grid(True)
    #     plt.show()

