#!/usr/bin/env python3
# A15736154 #PID

# import all modules here if you need any
import matplotlib.pyplot as plt
import numpy as np
from auxiliary_functions_edits import inCollision, dist


class Node:
    """ 
    Node class which has the x and y position along with the parent
    """    
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.parent = None

class RRT:
    """
    RRT class implementation
    """
    def __init__(self, start, goal, obstacle_list):
        self.start = Node(start[0], start[1])  # start node for the RRT
        self.goal = Node(goal[0], goal[1])     # goal node for the RRT
        self.obstacle_list = obstacle_list     # list of obstacles 
        self.node_list = []    # list of nodes added while creating the RRT
        self.node_list = [self.start]

    def planning(self, animation=True, goal_check=0.1, alpha=0.75):
        # self.node_list = [self.start]
        count = 0
        while self.goal.parent is None:
            if np.random.rand() > goal_check:
                q_random = np.random.sample((2, 1))*10.1
            else:
                q_random = np.asarray([self.goal.x, self.goal.y], dtype=float)
            # print("node class syntax: ", [self.start.x, self.start.y])
            print(type(Node.x))



            if animation:
                self.drawGraph(q_random)

            count = count+1
            if count > 500:
                break    

        path = [[self.goal.x, self.goal.y]]
        prev_node_index = len(self.node_list) - 1
        while self.node_list[prev_node_index].parent is not None:
            node = self.node_list[prev_node_index]
            path.insert(0, [node.x, node.y])
            prev_node_index = node.parent
        path.insert(0, [self.start.x, self.start.y])
        return path

    def calcDistNodeToPoint(self, node, point):
        """
        Input:  node as defined by node class
                point defined as a list (x,y)
        Return: distance between the node and point
        """
        # ##############
        # TODO: implement a method to calculate distance
        # and use it in getNearestNode
        # ##############
        node_to_point_distance = dist(node, point)
        return node_to_point_distance

    def getNearestNode(self, random_point):
        """
        Input: random_point which you sampled as (x,y)
        Return: index--index of the node in self.node_list 
                close_node-- the closest node as defined by node class 
        """
        index = 0  # to be revised
        dmin = self.calcDistNodeToPoint(self.node_list[0], random_point)
        
        # ##############
        # TODO: implement a method to find the closest node to the random_point
        # and use it in planning
        # ##############
        
        return index, close_node

    def drawGraph(self, random_point=None):
        """ Draw graph
        edit this function at your own risk
        """
        plt.clf()
        # draw random point
        if random_point is not None:
            plt.plot(random_point[0], random_point[1], "^k")
        # draw the tree
        for node in self.node_list:
            if node.parent is not None:
                plt.plot([node.x, self.node_list[node.parent].x], [
                         node.y, self.node_list[node.parent].y], "-g")
        # draw the obstacle
        for obstacle in self.obstacle_list:
            obstacle_draw = plt.Polygon(obstacle, fc="b")
            plt.gca().add_patch(obstacle_draw)
        # draw the start and goal points
        plt.plot(self.start.x, self.start.y, "xr")
        plt.plot(self.goal.x, self.goal.y, "xr")
        plt.axis([0, 10, 0, 10])
        plt.grid(True)
        plt.pause(0.01)

if __name__ == '__main__':
    '''
        You need to complete this planning part of the code, note that it has a
        random sampling part that outputs a random_point, but still you need
        to try to connect this random_point to the tree, by finding the
        closest node, and verify that the segment connecting to the tree
        is collision free. To check for collisions use an imported auxiliary
        function.
    
        Task 1: Find the nearest node to the sampled point
    
        Task 2: Find the distance to that node and if distance is greater than 
                0.25 use logic mentioned in the question to find the new point.
    
        Task 3: Check if the new point connects to the tree you are building
                without hitting an obstacle. You can use the auxiliary function
                to do this.
    
        Task 4: If collision free from the tree to the new point. Make this
                point a node in the tree with its parent as the nearest node
                to the tree.
        
        You can define any other methods in for the rrt object (i.e. functions
        that apply to the rrt object attributes) and which can help you
        complete the assignment, for example to obtain q nearby
    '''

    # Define obstacle polygon in the counter clockwise direction
    my_obstacle_list = [[[3, 3], [4, 3], [4, 4], [3, 4]], [[8, 8], [7, 6], [9, 9]]]

    # Set Initial parameters
    rrt = RRT(start=[2, 2], goal=[5, 5], obstacle_list=my_obstacle_list)
    
    show_animation = False
    check_goal = 0.1
    my_alpha = 0.75
    path = rrt.planning(animation=show_animation, goal_check=check_goal, alpha=my_alpha)
    print(path)
    # Draw final path
    if show_animation:
        rrt.drawGraph()
        plt.plot([x for (x, y) in path], [y for (x, y) in path], '-r')
        plt.grid(True)
        plt.show()
