#!/usr/bin/env python3
# A15736154 #PID

# import all modules here if you need any
import matplotlib.pyplot as plt
import numpy as np
import math
from auxiliary_functions import inCollision, isPointInConvexPolygon, dist


class Node:
    """
    Node class which has the x and y position along with the parent
    """
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.parent = None


class RRT:

    def __init__(self, start, goal, obstacle_list):
        (xs, ys) = start
        (xg, yg) = goal
        self.start = start
        self.goal = goal
        self.x = []
        self.y = []
        self.parent = []
        self.x.append(xs)
        self.y.append(ys)
        self.parent.append(0)
        self.obstacles = obstacle_list
        self.node_list = []
        self.node_list.append(Node(xs, ys))
        self.goalFlag = False
        self.goalState = None
        self.path = []
        self.nodePath = []
        self.alpha = 0.75

    def add_node(self, n, x, y):
        self.x.insert(n, x)
        self.y.insert(n, y)
        self.node_list.append(Node(x, y))

    def remove_node(self, n):
        self.x.pop(n)
        self.y.pop(n)

    def sample_envir(self):
        qx_random = np.random.sample() * 10.1
        qy_random = np.random.sample() * 10.1
        q_random = [qx_random, qy_random]
        # print(q_random)
        return q_random

    def add_edge(self, parent, child):
        self.parent.insert(child, parent)

    def remove_edge(self, n):
        self.parent.pop(n)

    def connect(self, n1, n2):
        if self.crossObstacle(n1, n2):
            self.remove_node(n2)
            return False
        else:
            self.add_edge(n1, n2)
            return True

    def number_of_nodes(self):
        n = len(self.x)
        return n

    def calcDistNodeToPoint(self, n1, n2):
        # try:
        (x1, y1) = (self.x[n1], self.y[n1])
        (x2, y2) = (self.x[n2], self.y[n2])
        # except TypeError:
        #     (x1, y1) = (n1.x, n1.y)
        #     (x2, y2) = (n2[0], n2[1])
        d = math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
        return d

    def getNearestNode(self, n):
        # try:
        dmin = self.calcDistNodeToPoint(0, n)
        # except AttributeError:
        #     dmin = self.calcDistNodeToPoint(n, self.start)
        #     print("Attribute error:", n)
        n = self.number_of_nodes()
        n_near = 0
        close_node = self.node_list[n_near]
        for i in range(0, n):
            if self.calcDistNodeToPoint(i, n) < dmin:
                dmin = self.calcDistNodeToPoint(i, n)
                n_near = i
                # x_near, y_near = [float(self.x[n_near]), float(self.y[n_near])]
                close_node = self.node_list[n_near]
        return n_near, close_node

    def inObstacle(self):
        [xr, yr] = self.sample_envir()
        obstacles_copy = self.obstacles.copy()
        for obstacle in obstacles_copy:
            check_collision = isPointInConvexPolygon([xr, yr], obstacle)
        return check_collision

    def crossObstacle(self, n1, n2):
        [x1, y1] = (self.x[n1], self.y[n1])
        [x2, y2] = (self.x[n2], self.y[n2])
        # print("test cross", [n1, n2])
        obstacles_copy = self.obstacles.copy()
        check_collision = inCollision([x1, y1], [x2, y2], obstacles_copy)
        return check_collision

    def step(self, n_near, n_rand, dmax=0.25):
        d = self.calcDistNodeToPoint(n_near, n_rand)
        try:
            if d > dmax:
                # u = dmax / d
                (x_near, y_near) = (self.x[n_near], self.y[n_near])
                (x_rand, y_rand) = (self.x[n_rand], self.y[n_rand])
                (x_new, y_new) = ((x_near * self.alpha + (1 - self.alpha)*x_rand), (y_near * self.alpha + (1 - self.alpha)*y_rand))
                self.remove_node(n_rand)
                d_check = dist([x_new, y_new], [self.goal[0], self.goal[1]])
                if abs(x_new - self.goal[0]) < dmax and abs(y_new - self.goal[1]) < dmax:
                    self.add_node(n_rand, self.goal[0], self.goal[1])
                    self.goalFlag = True
                    self.goalState = n_rand
                    # print("testing")
                else:
                    self.add_node(n_rand, x_new, y_new)
            else:
                d_check = "too large"
        except ZeroDivisionError:
            d_check = "zero"
        return d_check

    def bias(self, ngoal):
        n = self.number_of_nodes()
        self.add_node(n, ngoal[0], ngoal[1])
        n_near, dummy_variable = self.getNearestNode(n)
        d_check = self.step(n_near, n)
        self.connect(n_near, n)
        return self.x, self.y, self.parent, d_check

    def expand(self):
        n = self.number_of_nodes()
        x, y = self.sample_envir()
        self.add_node(n, x, y)

        if not self.inObstacle():
            q_nearest, dummy_variable = self.getNearestNode(n)
            # print([dummy_variable.x,  dummy_variable.y])
            d_check = self.step(q_nearest, n)
            self.connect(q_nearest, n)
        else:
            d_check = "in obstacle"
        return self.x, self.y, self.parent, d_check

    def pathToGoal(self):
        if self.goalFlag:
            self.path = []
            self.path.append(self.goalState)
            try:
                newPos = self.parent[self.goalState]
                while newPos != 0:
                    self.path.append(newPos)
                    newPos = self.parent[newPos]
                    self.nodePath.append(newPos)
                self.path.append(0)
            except IndexError:
                print("index error")
        return self.goalFlag

    def getPathCoordinates(self):
        x_pathCoordinates = []
        y_pathCoordinates = []
        self.pathToGoal()
        print("node Path: ", self.nodePath)
        print("Path: ", self.path)
        print("goal state parent: ", self.parent[self.goalState])
        print("parent list: ", self.parent)
        print("parent length: ", len(self.parent))
        print("goal state: ", self.goalState)
        for node in self.path:
            x = self.x[node]
            y = self.y[node]
            x_pathCoordinates.append(x)
            y_pathCoordinates.append(y)
        return x_pathCoordinates, y_pathCoordinates

    def drawPath(self, pathToGoal):
        pass
        # for node in path:

    def cost(self):
        pass

    def planning(self, animation, goal_check, alpha):
        iteration = 0
        self.alpha = alpha
        while iteration < 500:
            while not self.goalFlag:
                if animation:
                    try:
                        n = self.number_of_nodes()
                        plt.clf()
                        if iteration % int(1/goal_check) == 0:
                            X, Y, Parent, d_check = self.bias(self.goal)
                            # print(X,Y)
                            print("bias: ", d_check)
                        else:
                            X, Y, Parent, d_check = self.expand()
                            print("expand: ", d_check)
                        for obstacle in self.obstacles:
                            obstacle_draw = plt.Polygon(obstacle, fc="b")
                            plt.gca().add_patch(obstacle_draw)
                        for point in self.parent:
                            X_tree = [X[point], X[Parent[point]]]
                            Y_tree = [Y[point], Y[Parent[point]]]
                            plt.plot(X_tree, Y_tree, "b")
                            plt.scatter(X_tree, Y_tree, color="b")
                        # plt.scatter(self.x, self.y)
                        plt.plot(self.start[0], self.start[1], "xr")
                        plt.plot(self.goal[0], self.goal[0], "xr")
                        plt.axis([0, 10, 0, 10])
                        plt.grid(True)
                        plt.pause(0.01)
                    except IndexError:
                        print("Index Error")

                    # self.drawGraph()
                iteration = iteration + 1
                # print(self.goalFlag)
                if iteration > 500:
                    print("max iterations test")
                    return False
            iteration = 500
            X_path, Y_path = self.getPathCoordinates()
            # print(self.goalState)
            # print(n)
            print(X_path, Y_path)
            plt.plot(X_path, Y_path, 'r')
            plt.scatter(X_path, Y_path, color='r')
            print("goal reached")
            print(self.node_list)
        return True

    def drawGraph(self):
        """ Draw graph
        edit this function at your own risk
        """
        plt.clf()
        # draw random point
        # plt.plot(self.sample_envir(), "^k")
        # print(self.x)
        # print("testing", self.parent[self.x[-1]])
        # plt.plot([self.x[-1], self.x[self.parent[-1]]], [self.y[-1], self.y[self.parent[-1]]])
        # x_data =[]
        # x_parent_data = []
        # y_data = []
        # y_parent_data = []
        # x_data.append(self.x[-1])
        # x_parent_data.append(self.x[self.parent[-1]])
        # y_data.append(self.y[-1])
        # y_parent_data.append(self.y[self.parent[-1]])
        # plt.plot([x_data, x_parent_data],[y_data, y_parent_data])
        # print(self.parent)
        # print([self.x, self.x])

        # plt.scatter([X, X[Parent]], [Y, Y[Parent]])
        # draw the obstacle
        for obstacle in self.obstacles:
            obstacle_draw = plt.Polygon(obstacle, fc="b")
            plt.gca().add_patch(obstacle_draw)
        # draw the start and goal points
        plt.plot(self.start[0], self.start[1], "xr")
        plt.plot(self.goal[0], self.goal[0], "xr")
        plt.axis([0, 10, 0, 10])
        plt.grid(True)
        plt.pause(0.01)


if __name__ == '__main__':
    '''
        You need to complete this planning part of the code, note that it has a
        random sampling part that outputs a random_point, but still you need
        to try to connect this random_point to the tree, by finding the
        closest node, and verify that the segment connecting to the tree
        is collision free. To check for collisions use an imported auxiliary
        function.

        Task 1: Find the nearest node to the sampled point

        Task 2: Find the distance to that node and if distance is greater than 
                0.25 use logic mentioned in the question to find the new point.

        Task 3: Check if the new point connects to the tree you are building
                without hitting an obstacle. You can use the auxiliary function
                to do this.

        Task 4: If collision free from the tree to the new point. Make this
                point a node in the tree with its parent as the nearest node
                to the tree.

        You can define any other methods in for the rrt object (i.e. functions
        that apply to the rrt object attributes) and which can help you
        complete the assignment, for example to obtain q nearby
    '''

    # Define obstacle polygon in the counter clockwise direction
    my_obstacle_list = [[[3, 3], [4, 3], [4, 4], [3, 4]], [[8, 8], [7, 6], [9, 9]]]

    # Set Initial parameters
    rrt = RRT(start=[2, 2], goal=[5, 5], obstacle_list=my_obstacle_list)

    show_animation = True
    check_goal = 0.1
    my_alpha = 0.75
    path = rrt.planning(animation=show_animation, goal_check=check_goal,alpha=my_alpha)
    # n = rrt.number_of_nodes()
    # my_node_test = Node(2,2)
    # my_node_test2 = Node(1,1)
    # x_test = my_node_test.x
    # y_test = my_node_test.y
    # x_test2 = my_node_test2.x
    # y_test2 = my_node_test2.y
    # print(n, x_test, y_test)
    # rrt.add_node(n+1, x_test, y_test)
    # rrt.add_node(n+2, x_test2, y_test2)
    # # print("my_node_test: ", [x, y, my_node_test.parent])
    # # # test = rrt.calcDistNodeToPoint(my_node_test, [1,1])
    # test2 = rrt.getNearestNode(my_node_test)
    # print("my test: ", test2)
    # print(rrt.x)
    # Draw final path
    if show_animation:
        rrt.drawGraph()
        plt.grid(True)
        plt.show()






        '''
        
                        # if self.inObstacle():
                        #     self.remove_node(n)
                        # if self.crossObstacle(n - 1, n):
                        #     self.remove_node(n)
                        # else:
                        #     self.add_edge(n - 1, n)'''